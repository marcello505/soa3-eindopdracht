using System;
using System.Collections.Generic;
using System.Linq;
using Core.Domain;
using Core.Domain.DevOps;
using Core.Domain.DevOps.Components;
using Core.Domain.DevOps.Components.Debug;
using Core.Domain.DevOps.Components.Utility;
using Core.Domain.DevOps.Visitors;
using Core.Domain.Forum;
using Core.Domain.Notifications;
using Core.Domain.ProductBacklogItems;
using Core.Domain.TypesOfSprints.ReleaseSprints;
using Core.Domain.TypesOfUser;
using Xunit;

namespace Core.Tests;

public class NotificationTests
{
    private const string DUMMY_STRING = "DUMMY";

    [Fact]
    public void TC1201_ScrumMasterReceivesNotificationOnPbiStateChangeFromRftToTodo()
    {
        //Arrange
        var project = SampleProject();
        var sprint = project.SprintBacklogs.First();
        var pbi = sprint.Items.First();
        pbi.State = new ReadyForTestingState(pbi);
        var tester = sprint.Team.Single(u => u is Tester);
        var scrumMaster = sprint.Team.Single(u => u is ScrumMaster);
        tester.AddNotificationType(new EmailSubscriber(DUMMY_STRING));
        scrumMaster.AddNotificationType(new SmsSubscriber(DUMMY_STRING));
        scrumMaster.AddNotificationType(new SlackSubscriber(DUMMY_STRING));
        
        //Act
        pbi.State.ToDo(tester);
        
        //Assert
        Assert.True(scrumMaster.NotificationTypes.First().NotificationReceived);
        Assert.True(scrumMaster.NotificationTypes.Last().NotificationReceived);
        Assert.False(tester.NotificationTypes.First().NotificationReceived);
    }

    [Fact]
    public void TC1301_TestersGetAlertedWhenPbiEntersRftStateFromDoing()
    {
        //Arrange
        var project = SampleProject();
        var sprint = project.SprintBacklogs.First();
        var pbi = sprint.Items.First();
        pbi.State = new DoingState(pbi);
        var developer = sprint.Team.Single(u => u is Developer);
        var tester = sprint.Team.Single(u => u is Tester);
        developer.AddNotificationType(new EmailSubscriber(DUMMY_STRING));
        tester.AddNotificationType(new SmsSubscriber(DUMMY_STRING));
        
        //Act
        pbi.State.ReadyForTesting(developer);
        
        //Assert
        Assert.True(tester.NotificationTypes.First().NotificationReceived);
        Assert.False(developer.NotificationTypes.First().NotificationReceived);
    }
    
    [Fact]
    public void TC1302_TestersGetAlertedWhenPbiEntersRftStateFromTested()
    {
        //Arrange
        var project = SampleProject();
        var sprint = project.SprintBacklogs.First();
        var pbi = sprint.Items.First();
        pbi.State = new TestedState(pbi);
        var scrumMaster = sprint.Team.Single(u => u is ScrumMaster);
        var tester = sprint.Team.Single(u => u is Tester);
        scrumMaster.AddNotificationType(new EmailSubscriber(DUMMY_STRING));
        tester.AddNotificationType(new EmailSubscriber(DUMMY_STRING));
        
        //Act
        pbi.State.ReadyForTesting(scrumMaster);
        
        //Assert
        Assert.True(tester.NotificationTypes.First().NotificationReceived);
        Assert.False(scrumMaster.NotificationTypes.First().NotificationReceived);
    }

    [Fact]
    public void TC1401_ScrumMasterReceivesNotificationOnUnsuccessfulDevelopmentPipelineRun()
    {
        //Arrange
        var project = SampleProject();
        var sprint = project.SprintBacklogs.First();
        var pipeline = new DevelopmentPipeline(DUMMY_STRING);
        sprint.SetPipeline(pipeline);
        var job = new Job(DUMMY_STRING);
        job.Add(new Crash());
        pipeline.addJob(job);
        var developer = sprint.Team.Single(u => u is Developer);
        var scrumMaster = sprint.Team.Single(u => u is ScrumMaster);
        developer.AddNotificationType(new EmailSubscriber(DUMMY_STRING));
        scrumMaster.AddNotificationType(new SmsSubscriber(DUMMY_STRING));
        
        //Act
        var operationCount = new OperationCountVisitor();
        pipeline.accept(operationCount);
        
        //Assert
        Assert.Equal(DevelopmentPipeline.PipelineStatusEnum.Failed, pipeline.PipelineStatus);
        Assert.True(scrumMaster.NotificationTypes.First().NotificationReceived);
        Assert.False(developer.NotificationTypes.First().NotificationReceived);
    }
    
    [Fact]
    public void TC1402_ScrumMasterReceivesNoNotificationOnSuccessfulDevelopmentPipelineRun()
    {
        //Arrange
        var project = SampleProject();
        var sprint = project.SprintBacklogs.First();
        var pipeline = new DevelopmentPipeline(DUMMY_STRING);
        sprint.SetPipeline(pipeline);
        var job = new Job(DUMMY_STRING);
        job.Add(new CopyFile(DUMMY_STRING, DUMMY_STRING));
        pipeline.addJob(job);
        var scrumMaster = sprint.Team.Single(u => u is ScrumMaster);
        scrumMaster.AddNotificationType(new SmsSubscriber(DUMMY_STRING));
        
        //Act
        var operationCount = new OperationCountVisitor();
        pipeline.accept(operationCount);
        
        //Assert
        Assert.Equal(DevelopmentPipeline.PipelineStatusEnum.Success, pipeline.PipelineStatus);
        Assert.False(scrumMaster.NotificationTypes.First().NotificationReceived);
    }

    [Fact]
    public void TC1501_ProductOwnerAndScrumMasterReceiveNotificationOnSprintReleaseCompleted()
    {
        //Arrange
        var project = SampleProject();
        var sprint = (ReleaseSprint)project.SprintBacklogs.First();
        var pipeline = new DevelopmentPipeline(DUMMY_STRING);
        sprint.SetPipeline(pipeline);
        sprint.State = new FinishedState(sprint);
        var scrumMaster = sprint.Team.Single(u => u is ScrumMaster);
        var productOwner = project.ProductOwner;
        var developer = sprint.Team.Single(u => u is Developer);
        scrumMaster.NotificationTypes.Add(new EmailSubscriber(DUMMY_STRING));
        productOwner.NotificationTypes.Add(new SmsSubscriber(DUMMY_STRING));
        developer.NotificationTypes.Add(new SlackSubscriber(DUMMY_STRING));

        //Act
        sprint.State.Release();
        
        //Assert
        Assert.IsType<CompletedState>(sprint.State);
        Assert.True(scrumMaster.NotificationTypes.First().NotificationReceived);
        Assert.True(productOwner.NotificationTypes.First().NotificationReceived);
        Assert.False(developer.NotificationTypes.First().NotificationReceived);
    }
    
    [Fact]
    public void TC1502_ProductOwnerAndScrumMasterReceiveNotificationOnSprintReleaseCancelled()
    {
        //Arrange
        var project = SampleProject();
        var sprint = (ReleaseSprint)project.SprintBacklogs.First();
        var pipeline = new DevelopmentPipeline(DUMMY_STRING);
        var job = new Job(DUMMY_STRING);
        job.Add(new Crash());
        pipeline.addJob(job);
        sprint.SetPipeline(pipeline);
        sprint.State = new FinishedState(sprint);
        var scrumMaster = sprint.Team.Single(u => u is ScrumMaster);
        var productOwner = project.ProductOwner;
        var developer = sprint.Team.Single(u => u is Developer);
        scrumMaster.NotificationTypes.Add(new EmailSubscriber(DUMMY_STRING));
        productOwner.NotificationTypes.Add(new SmsSubscriber(DUMMY_STRING));
        developer.NotificationTypes.Add(new SlackSubscriber(DUMMY_STRING));

        //Act
        sprint.State.Release();
        
        //Assert
        Assert.IsType<CancelledState>(sprint.State);
        Assert.True(scrumMaster.NotificationTypes.First().NotificationReceived);
        Assert.True(productOwner.NotificationTypes.First().NotificationReceived);
        Assert.False(developer.NotificationTypes.First().NotificationReceived);
    }

    [Fact]
    public void TC1601_UserSubscribesToThread()
    {
        //Arrange
        var threadStarterUser = new Developer(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        var threadStarterMessage = new Message(DUMMY_STRING, threadStarterUser);
        var threadJoinerUser = new Tester(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        var threadJoinerMessage = new Message(DUMMY_STRING, threadJoinerUser);
        var threadExplicitSubscriber = new ScrumMaster(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        var thread = new Thread(threadStarterMessage);
        
        //Act
        thread.AddMessage(threadJoinerMessage);
        thread.Subscribe(threadExplicitSubscriber);
        
        //Assert
        Assert.Contains(threadStarterUser, thread.SubscribedUsers);
        Assert.Contains(threadJoinerUser, thread.SubscribedUsers);
        Assert.Contains(threadExplicitSubscriber, thread.SubscribedUsers);
    }

    [Fact]
    public void TC1602_UserGetNotificationOnNewMessage()
    {
        //Arrange
        var poster = new Developer(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        poster.NotificationTypes.Add(new EmailSubscriber(DUMMY_STRING));
        var subscriber = new Tester(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        subscriber.NotificationTypes.Add(new SmsSubscriber(DUMMY_STRING));
        var thread = new Thread(new Message(DUMMY_STRING, poster));
        
        //Act
        thread.Subscribe(subscriber);
        thread.AddMessage(new Message(DUMMY_STRING, poster));
        
        //Assert
        Assert.False(poster.NotificationTypes.First().NotificationReceived);
        Assert.True(subscriber.NotificationTypes.First().NotificationReceived);
    }
    
    [Fact]
    public void TC1603_UserGetsNoNotificationsAfterUnsubscribing()
    {
        //Arrange
        var unsubscribedUser = new Developer(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        unsubscribedUser.NotificationTypes.Add(new EmailSubscriber(DUMMY_STRING));
        var otherPoster = new Tester(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        otherPoster.NotificationTypes.Add(new SmsSubscriber(DUMMY_STRING));
        var thread = new Thread(new Message(DUMMY_STRING, unsubscribedUser));
        
        //Act
        thread.Unsubscribe(unsubscribedUser);
        thread.AddMessage(new Message(DUMMY_STRING, otherPoster));
        
        //Assert
        Assert.False(unsubscribedUser.NotificationTypes.First().NotificationReceived);
    }


    private Project SampleProject()
    {
        //Team members
        var scrumMaster = new ScrumMaster(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        var developer = new Developer(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        var productOwner = new ProductOwner(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        var tester = new Tester(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        
        IList<User> teamMembers = new List<User>();
        teamMembers.Add(developer);
        teamMembers.Add(scrumMaster);
        teamMembers.Add(tester);

        //Project structure
        var project = new Project(DUMMY_STRING);
        project.ProductOwner = productOwner;
        var sprint = new ReleaseSprint(0, DateTime.Now, DateTime.Now.AddDays(7));
        sprint.Team.AddRange(teamMembers);
        var pbi = new ProductBacklogItem(DUMMY_STRING, 1);
        pbi.Developer = developer;
        project.AddSprint(sprint);
        sprint.AddItem(pbi);

        return project;
    }
}