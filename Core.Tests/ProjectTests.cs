﻿using Core.Domain;
using Core.Domain.ProductBacklogItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Xunit;
using Core.Domain.TypesOfUser;
using Core.Domain.TypesOfSprints.ReviewSprints;
using Core.Domain.TypesOfSprints.ReleaseSprints;

namespace Core.Tests
{
    public class ProjectTests
    {
        [Fact]
        public void TC0101CreateANewProjectCheckName()
        {
            // Arrange
            Project project = new Project("SOFA3");

            // Assert
            Assert.Equal("SOFA3", project.ProjectName);
        }

        [Fact]
        public void TC0102CreateANewProjectCheckVersion()
        {
            // Arrange
            Project project = new Project("SOFA3");

            // Assert
            Assert.Equal("0.0", project.Version);
        }

        [Fact]
        public void TC0103AddProductOwnerToProject()
        {
            // Arrange
            Project project = new Project("SOFA3");
            ProductOwner productOwner = new ProductOwner("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            project.AssignProductOwner(productOwner);

            // Assert
            Assert.Equal(productOwner, project.ProductOwner);
        }

        [Fact]
        public void TC0104CreateANewProjectWithEmptyList()
        {
            // Arrange
            Project project = new Project("SOFA3");

            // Assert
            Assert.Empty(project.ProductBacklog);
        }

        [Fact]
        public void TC0105AddOneItemToProductBacklogList()
        {
            // Arrange
            Project project = new Project("SOFA3");

            // Act
            project.AddItem(new Mock<ProductBacklogItem>("Als gebruiker wil ik een fase kunnen toekennen.", 1).Object);
            
            // Assert
            Assert.NotEmpty(project.ProductBacklog);
            Assert.Single(project.ProductBacklog);
        }

        [Fact]
        public void TC0106AddTwoItemsToProductBacklog()
        {
            // Arrange
            Project project = new Project("SOFA3");

            // Act
            project.AddItem(new Mock<ProductBacklogItem>("Als gebruiker wil ik een fase kunnen toekennen.", 1).Object);
            project.AddItem(new Mock<ProductBacklogItem>("Als gebruiker wil ik een fase kunnen toekennen.", 1).Object);

            // Assert
            Assert.Equal(2, project.ProductBacklog.Count);
        }

        [Fact]
        public void TC0107CreateAReviewSprint()
        {
            // Arrange
            Project project = new Project("SOFA3");
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);

            // Act
            project.CreateNewReviewSprint(1, startDate, endDate);

            // Assert
            Assert.NotEmpty(project.SprintBacklogs);
            Assert.IsType<ReviewSprint>(project.SprintBacklogs[0]);
        }

        [Fact]
        public void TC0108CreateAReleaseSprint()
        {
            // Arrange
            Project project = new Project("SOFA3");
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);

            // Act
            project.CreateNewReleaseSprint(1, startDate, endDate);

            // Assert
            Assert.NotEmpty(project.SprintBacklogs);
            Assert.IsType<ReleaseSprint>(project.SprintBacklogs[0]);
        }

        [Fact]
        public void TC0109ItemFromBacklogToSprint()
        {
            // Arrange
            Project project = new Project("SOFA3");
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 1);

            // Act
            project.AddItem(item);
            project.CreateNewReviewSprint(1, startDate, endDate);
            project.ItemToSprint(project.SprintBacklogs[0], project.ProductBacklog[0]);

            // Assert
            Assert.NotEmpty(project.SprintBacklogs[0].Items);
        }

        [Fact]
        public void TC0110UpdateVersionFromProject()
        {
            // Arrange
            Project project = new Project("SOFA3");

            // Act
            project.UpdateVersion("0.1");

            // Assert
            Assert.Equal("0.1", project.Version);
        }
    }
}
