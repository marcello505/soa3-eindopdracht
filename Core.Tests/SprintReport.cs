using System;
using Core.Domain;
using Core.Domain.ProductBacklogItems;
using Core.Domain.SprintReport;
using Core.Domain.SprintReport.Reports;
using Core.Domain.TypesOfSprints.ReleaseSprints;
using Core.Domain.TypesOfUser;
using Xunit;

namespace Core.Tests;

public class SprintReport
{
    private const string DUMMY_STRING = "DUMMY";
    private const string TEAMCOMP_IDENTIFIER = "<team>";
    private const string BURNDOWNCHART_IDENTIFIER = "<burndownchart />";
    private const string EFFORTPOINTOVERVIEW_IDENTIFIER = "<effortpointoverview>";
    private const string LOGO_IDENTIFIER = "<img";
    private const string PROJECTNAME_IDENTIFIER = "<h1>";
    private const string VERSION_IDENTIFIER = "<span id=\"version\">";
    private const string DATE_IDENTIFIER = "<span id=\"date\">";

    [Fact]
    public void TC1001GenerateReportInMultipleFormats()
    {
        //Arrange
        var pdfBuilder = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var imageBuilder = new ImageSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        BuildWithAllOptions(pdfBuilder);
        BuildWithAllOptions(imageBuilder);
        var pdfReport = pdfBuilder.GetResult();
        var imageReport = imageBuilder.GetResult(ImageReport.Extensions.Png);
        
        //Assert
        Assert.IsType<PdfReport>(pdfReport);
        Assert.IsNotType<ImageReport>(pdfReport);
        Assert.IsType<ImageReport>(imageReport);
        Assert.IsNotType<PdfReport>(imageReport);
    }

    [Fact]
    public void TC1002AddTeamComposition()
    {
        //Arrange
        var pdfBuilderWithTeamComp = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoTeamComp = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithTeamComp.AddTeamComposition();
        
        //Assert
        Assert.Contains(TEAMCOMP_IDENTIFIER, pdfBuilderWithTeamComp.GetResult().BodyContent);
        Assert.DoesNotContain(TEAMCOMP_IDENTIFIER, pdfBuilderWithNoTeamComp.GetResult().BodyContent);
    }
    
    [Fact]
    public void TC1003AddBurndownChart()
    {
        //Arrange
        var pdfBuilderWithBurnDownChart = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoBurnDownChart = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithBurnDownChart.AddBurnDownChart();
        
        //Assert
        Assert.Contains(BURNDOWNCHART_IDENTIFIER, pdfBuilderWithBurnDownChart.GetResult().BodyContent);
        Assert.DoesNotContain(BURNDOWNCHART_IDENTIFIER, pdfBuilderWithNoBurnDownChart.GetResult().BodyContent);
    }
    
    [Fact]
    public void TC1004AddEffortPointOverview()
    {
        //Arrange
        var pdfBuilderWithEffortPointOverview = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoEffortPointOverview = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithEffortPointOverview.AddEffortPointOverview();
        
        //Assert
        Assert.Contains(EFFORTPOINTOVERVIEW_IDENTIFIER, pdfBuilderWithEffortPointOverview.GetResult().BodyContent);
        Assert.DoesNotContain(EFFORTPOINTOVERVIEW_IDENTIFIER, pdfBuilderWithNoEffortPointOverview.GetResult().BodyContent);
    }
    
    [Fact]
    public void TC1005AddAllBodyContent()
    {
        //Arrange
        var pdfBuilderWithAllBodyContent = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithAllBodyContent.AddTeamComposition();
        pdfBuilderWithAllBodyContent.AddBurnDownChart();
        pdfBuilderWithAllBodyContent.AddEffortPointOverview();
        
        //Assert
        Assert.Contains(TEAMCOMP_IDENTIFIER, pdfBuilderWithAllBodyContent.GetResult().BodyContent);
        Assert.Contains(BURNDOWNCHART_IDENTIFIER, pdfBuilderWithAllBodyContent.GetResult().BodyContent);
        Assert.Contains(EFFORTPOINTOVERVIEW_IDENTIFIER, pdfBuilderWithAllBodyContent.GetResult().BodyContent);
    }
    
    [Fact]
    public void TC1006AddHeaderLogo()
    {
        //Arrange
        var pdfBuilderWithHeaderLogo = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoHeaderLogo = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithHeaderLogo.SetHeaderLogo(new byte[]{1,2,3,4,5});
        
        //Assert
        Assert.Contains(LOGO_IDENTIFIER, pdfBuilderWithHeaderLogo.GetResult().HeaderContent);
        Assert.DoesNotContain(LOGO_IDENTIFIER, pdfBuilderWithNoHeaderLogo.GetResult().HeaderContent);
    }
    
    [Fact]
    public void TC1007AddHeaderProjectName()
    {
        //Arrange
        var pdfBuilderWithHeaderProjectName = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoHeaderProjectName = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithHeaderProjectName.SetHeaderProjectName(DUMMY_STRING);
        
        //Assert
        Assert.Contains(PROJECTNAME_IDENTIFIER, pdfBuilderWithHeaderProjectName.GetResult().HeaderContent);
        Assert.DoesNotContain(PROJECTNAME_IDENTIFIER, pdfBuilderWithNoHeaderProjectName.GetResult().HeaderContent);
    }
    
    [Fact]
    public void TC1008AddHeaderVersionNumber()
    {
        //Arrange
        var pdfBuilderWithHeaderVersionNumber = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoHeaderVersionNumber = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithHeaderVersionNumber.SetHeaderVersion(DUMMY_STRING);
        
        //Assert
        Assert.Contains(VERSION_IDENTIFIER, pdfBuilderWithHeaderVersionNumber.GetResult().HeaderContent);
        Assert.DoesNotContain(VERSION_IDENTIFIER, pdfBuilderWithNoHeaderVersionNumber.GetResult().HeaderContent);
    }

    [Fact]
    public void TC1009AddHeaderDate()
    {
        //Arrange
        var pdfBuilderWithHeaderDate = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoHeaderDate = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithHeaderDate.SetHeaderDate(DateTime.Now);
        
        //Assert
        Assert.Contains(DATE_IDENTIFIER, pdfBuilderWithHeaderDate.GetResult().HeaderContent);
        Assert.DoesNotContain(DATE_IDENTIFIER, pdfBuilderWithNoHeaderDate.GetResult().HeaderContent);
    }
    
    [Fact]
    public void TC1010AddAllHeaderContent()
    {
        //Arrange
        var pdfBuilderWithAllHeaderContent = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithAllHeaderContent.SetHeaderDate(DateTime.Now);
        pdfBuilderWithAllHeaderContent.SetHeaderVersion(DUMMY_STRING);
        pdfBuilderWithAllHeaderContent.SetHeaderProjectName(DUMMY_STRING);
        pdfBuilderWithAllHeaderContent.SetHeaderLogo(new byte[]{1,2,3,4,5});
        
        //Assert
        Assert.Contains(DATE_IDENTIFIER, pdfBuilderWithAllHeaderContent.GetResult().HeaderContent);
        Assert.Contains(VERSION_IDENTIFIER, pdfBuilderWithAllHeaderContent.GetResult().HeaderContent);
        Assert.Contains(PROJECTNAME_IDENTIFIER, pdfBuilderWithAllHeaderContent.GetResult().HeaderContent);
        Assert.Contains(LOGO_IDENTIFIER, pdfBuilderWithAllHeaderContent.GetResult().HeaderContent);
    }
    
    [Fact]
    public void TC1011AddFooterLogo()
    {
        //Arrange
        var pdfBuilderWithFooterLogo = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoFooterLogo = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithFooterLogo.SetFooterLogo(new byte[]{1,2,3,4,5});
        
        //Assert
        Assert.Contains(LOGO_IDENTIFIER, pdfBuilderWithFooterLogo.GetResult().FooterContent);
        Assert.DoesNotContain(LOGO_IDENTIFIER, pdfBuilderWithNoFooterLogo.GetResult().FooterContent);
    }
    
    [Fact]
    public void TC1012AddFooterProjectName()
    {
        //Arrange
        var pdfBuilderWithFooterProjectName = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoFooterProjectName = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithFooterProjectName.SetFooterProjectName(DUMMY_STRING);
        
        //Assert
        Assert.Contains(PROJECTNAME_IDENTIFIER, pdfBuilderWithFooterProjectName.GetResult().FooterContent);
        Assert.DoesNotContain(PROJECTNAME_IDENTIFIER, pdfBuilderWithNoFooterProjectName.GetResult().FooterContent);
    }
    
    [Fact]
    public void TC1013AddFooterVersionNumber()
    {
        //Arrange
        var pdfBuilderWithFooterVersionNumber = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoFooterVersionNumber = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithFooterVersionNumber.SetFooterVersion(DUMMY_STRING);
        
        //Assert
        Assert.Contains(VERSION_IDENTIFIER, pdfBuilderWithFooterVersionNumber.GetResult().FooterContent);
        Assert.DoesNotContain(VERSION_IDENTIFIER, pdfBuilderWithNoFooterVersionNumber.GetResult().FooterContent);
    }

    [Fact]
    public void TC1014AddFooterDate()
    {
        //Arrange
        var pdfBuilderWithFooterDate = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        var pdfBuilderWithNoFooterDate = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithFooterDate.SetFooterDate(DateTime.Now);
        
        //Assert
        Assert.Contains(DATE_IDENTIFIER, pdfBuilderWithFooterDate.GetResult().FooterContent);
        Assert.DoesNotContain(DATE_IDENTIFIER, pdfBuilderWithNoFooterDate.GetResult().FooterContent);
    }
    
    [Fact]
    public void TC1015AddAllFooterContent()
    {
        //Arrange
        var pdfBuilderWithAllFooterContent = new PdfSprintReportBuilder(GetSampleSprintBackLog());
        
        //Act
        pdfBuilderWithAllFooterContent.SetFooterDate(DateTime.Now);
        pdfBuilderWithAllFooterContent.SetFooterVersion(DUMMY_STRING);
        pdfBuilderWithAllFooterContent.SetFooterProjectName(DUMMY_STRING);
        pdfBuilderWithAllFooterContent.SetFooterLogo(new byte[]{1,2,3,4,5});
        
        //Assert
        Assert.Contains(DATE_IDENTIFIER, pdfBuilderWithAllFooterContent.GetResult().FooterContent);
        Assert.Contains(VERSION_IDENTIFIER, pdfBuilderWithAllFooterContent.GetResult().FooterContent);
        Assert.Contains(PROJECTNAME_IDENTIFIER, pdfBuilderWithAllFooterContent.GetResult().FooterContent);
        Assert.Contains(LOGO_IDENTIFIER, pdfBuilderWithAllFooterContent.GetResult().FooterContent);
    }

    [Fact]
    public void TC1016CalculateEffortPointOverviewCorrectly()
    {
        //Arrange
        const string UNIQUE_STRING = "UNIQUE";
        var sprintBacklog = GetSampleSprintBackLog();
        var productBacklogItemNotDone = new ProductBacklogItem(DUMMY_STRING, 5);
        var productBacklogItemDone = new ProductBacklogItem(DUMMY_STRING, 5);
        var uniqueDev = new Developer(UNIQUE_STRING, UNIQUE_STRING, UNIQUE_STRING);
        var pdfBuilder = new PdfSprintReportBuilder(sprintBacklog);
        
        productBacklogItemDone.Developer = uniqueDev;
        productBacklogItemNotDone.Developer = uniqueDev;
        productBacklogItemDone.State = new DoneState(productBacklogItemDone);
        sprintBacklog.AddItem(productBacklogItemDone);
        sprintBacklog.AddItem(productBacklogItemNotDone);
        
        //Act
        pdfBuilder.AddEffortPointOverview();
        
        //Assert
        Assert.Contains($"{UNIQUE_STRING} - {5} points", pdfBuilder.GetResult().BodyContent);
    }
    
    private void BuildWithAllOptions(AbstractSprintReportBuilder builder)
    {
        builder.Reset();
        builder.AddTeamComposition();
        builder.AddBurnDownChart();
        builder.AddEffortPointOverview();
        builder.SetHeaderProjectName(DUMMY_STRING);
        builder.SetHeaderLogo(new byte[]{0,1,2,3,4,5});
        builder.SetHeaderDate(DateTime.Now);
        builder.SetHeaderVersion(DUMMY_STRING);
        builder.SetFooterProjectName(DUMMY_STRING);
        builder.SetFooterLogo(new byte[]{0,1,2,3,4,5});
        builder.SetFooterDate(DateTime.Now);
        builder.SetFooterVersion(DUMMY_STRING);
    }

    private SprintBacklog GetSampleSprintBackLog()
    {
        //create
        var sprintBacklog = new ReleaseSprint(1, DateTime.Now, DateTime.Now.AddDays(7));
        var scrumMaster = new ScrumMaster(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        var developer = new Developer(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
        var pbi1 = new ProductBacklogItem(DUMMY_STRING, 1);
        var pbi2 = new ProductBacklogItem(DUMMY_STRING, 1);
        
        
        //assign
        sprintBacklog.Team.Add(scrumMaster);
        sprintBacklog.Team.Add(developer);
        sprintBacklog.AddItem(pbi1);
        sprintBacklog.AddItem(pbi2);
        pbi1.Developer = developer;
        pbi1.State = new DoneState(pbi1);
        pbi2.Developer = developer;
        pbi2.State = new DoneState(pbi2);
        return sprintBacklog;
    }
}