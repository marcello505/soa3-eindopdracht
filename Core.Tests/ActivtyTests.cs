﻿using Core.Domain.Activities;
using Core.Domain.Activities.States;
using Core.Domain.ProductBacklogItems;
using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using DoingState = Core.Domain.ProductBacklogItems.DoingState;
using DoneState = Core.Domain.ProductBacklogItems.DoneState;

namespace Core.Tests
{
    public class ActivtyTests
    {
        public ProductOwner productOwner = new ProductOwner("Arno", "Broeders", "a.broeders@avans.nl");
        public ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");
        public Tester tester = new Tester("Arno", "Broeders", "a.broeders@avans.nl");
        public Developer developer = new Developer("Arno", "Broeders", "a.broeders@avans.nl");

        private const string DUMMY_STRING = "DUMMY";

        [Fact]
        public void TC0301CreateActivityAndLinkToProductBacklogItem()
        {
            // Arrange
            var pbi = new ProductBacklogItem(DUMMY_STRING, 1);
            var activity = new Activity(DUMMY_STRING);

            // Act
            pbi.AddActivity(activity);

            // Assert
            Assert.Contains(activity, pbi.Activities);
            Assert.IsType<TodoState>(activity.State);
        }

        [Fact]
        public void TC0302ActivityStateSwitchFromTodoToDoing()
        {
            // Arrange
            var activity = new Activity(DUMMY_STRING);
            var developer = new Developer(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);

            // ActAndAssert
            Assert.IsType<TodoState>(activity.State);
            Assert.Throws<InvalidOperationException>(activity.State.Doing);
            activity.Developer = developer;
            activity.State.Doing();
            Assert.IsType<Core.Domain.Activities.States.DoingState>(activity.State);
        }

        [Fact]
        public void TC0303ActivityStateSwitchFromDoingToDone()
        {
            // Arrange
            var activity = new Activity(DUMMY_STRING);
            activity.Developer = new Developer(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);

            // ActAndAssert
            Assert.IsType<TodoState>(activity.State);
            Assert.Throws<InvalidOperationException>(activity.State.Done);
            activity.State.Doing();
            Assert.IsType<Core.Domain.Activities.States.DoingState>(activity.State);
            activity.State.Done();
            Assert.IsType<Core.Domain.Activities.States.DoneState>(activity.State);
        }

        [Fact]
        public void TC0304ActivityStateSwitchFromDoneToTodo()
        {
            // Arrange
            var activity = new Activity(DUMMY_STRING);
            activity.Developer = new Developer(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);

            // ActAndAssert
            ////State is "TodoState"
            Assert.IsType<TodoState>(activity.State);
            activity.State.Doing();
            ////State is "DoingState"
            Assert.IsType<Core.Domain.Activities.States.DoingState>(activity.State);
            Assert.Throws<InvalidOperationException>(activity.State.Todo);
            activity.State.Done();
            ////State is "DoneState"
            Assert.IsType<Core.Domain.Activities.States.DoneState>(activity.State);
            Assert.Throws<InvalidOperationException>(activity.State.Doing);
            activity.State.Todo();
            Assert.IsType<TodoState>(activity.State);
        }

        [Fact]
        public void TC0305ActivityStateSwitchingDoesntWorkWhenSwitchingToCurrentState()
        {
            //Arrange
            var activity = new Activity(DUMMY_STRING);
            activity.Developer = new Developer(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);

            // ActAndAssert
            Assert.Throws<InvalidOperationException>(activity.State.Todo);
            activity.State.Doing();
            Assert.Throws<InvalidOperationException>(activity.State.Doing);
            activity.State.Done();
            Assert.Throws<InvalidOperationException>(activity.State.Done);
        }

        [Fact]
        public void TC0306ProductBacklogItemCanOnlyBeDoneWhenAllActivitiesAreDone()
        {
            //Arrange
            var pbiWithDoneActivity = new ProductBacklogItem(DUMMY_STRING, 1);
            var pbiWithNotDoneActivity = new ProductBacklogItem(DUMMY_STRING, 1);
            var pbiWithNoActivity = new ProductBacklogItem(DUMMY_STRING, 1);
            var activityDone = new Activity(DUMMY_STRING);
            var activityNotDone = new Activity(DUMMY_STRING);
            var developer = new Developer(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING);
            pbiWithDoneActivity.Developer = developer;
            pbiWithNotDoneActivity.Developer = developer;
            pbiWithNoActivity.Developer = developer;
            pbiWithDoneActivity.AddActivity(activityDone);
            pbiWithNotDoneActivity.AddActivity(activityNotDone);
            activityDone.Developer = developer;
            activityNotDone.Developer = developer;

            // Act
            activityDone.State.Doing();
            activityDone.State.Done();
            pbiWithDoneActivity.State.Doing();
            pbiWithDoneActivity.State.ReadyForTesting(developer);
            pbiWithDoneActivity.State.Testing();
            pbiWithDoneActivity.State.Tested();
            pbiWithNotDoneActivity.State.Doing();
            pbiWithNotDoneActivity.State.ReadyForTesting(developer);
            pbiWithNotDoneActivity.State.Testing();
            pbiWithNotDoneActivity.State.Tested();
            pbiWithNoActivity.State.Doing();
            pbiWithNoActivity.State.ReadyForTesting(developer);
            pbiWithNoActivity.State.Testing();
            pbiWithNoActivity.State.Tested();

            //Assert
            pbiWithDoneActivity.State.Done(scrumMaster);
            pbiWithNotDoneActivity.State.Done(scrumMaster);
            pbiWithNoActivity.State.Done(scrumMaster);
            Assert.IsType<DoneState>(pbiWithDoneActivity.State);
            Assert.IsType<DoneState>(pbiWithNoActivity.State);
            Assert.IsType<TestedState>(pbiWithNotDoneActivity.State);
        }
    }
}
