﻿using Core.Domain.ProductBacklogItems;
using Core.Domain.TypesOfUser;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.Activities;
using Core.Domain.Activities.States;
using Xunit;
using DoingState = Core.Domain.ProductBacklogItems.DoingState;
using DoneState = Core.Domain.ProductBacklogItems.DoneState;
using Core.Domain;

namespace Core.Tests
{
    public class ProductBacklogItemTests
    {
        public ProductOwner productOwner = new ProductOwner("Arno", "Broeders", "a.broeders@avans.nl");
        public ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");
        public Tester tester = new Tester("Arno", "Broeders", "a.broeders@avans.nl");
        public Developer developer = new Developer("Arno", "Broeders", "a.broeders@avans.nl");
        
        [Fact]
        public void TC0111AssignDeveloperToItem()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 1);
            Developer user = new Developer("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            item.AssignDeveloper(user);

            // Assert
            Assert.NotNull(item.Developer);
        }

        [Fact]
        public void TC0201StateOfNewProductBacklogItemIsToDoState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Assert
            Assert.IsType<ToDoState>(item.State);
        }

        [Fact]
        public void TC0202ProductBacklogItemFromToDoStateToDoingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();

            // Assert
            Assert.IsType<DoingState>(item.State);
        }

        [Fact]
        public void TC0203ProductBacklogItemFromToDoStateToReadyForTestingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.ReadyForTesting(developer);

            // Assert
            Assert.IsType<ToDoState>(item.State);
        }

        [Fact]
        public void TC0204ProductBacklogItemFromToDoStateToTestingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Testing();

            // Assert
            Assert.IsType<ToDoState>(item.State);
        }

        [Fact]
        public void TC0205ProductBacklogItemFromToDoStateToTestedState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Tested();

            // Assert
            Assert.IsType<ToDoState>(item.State);
        }

        [Fact]
        public void TC0206ProductBacklogItemFromToDoStateToDoneState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Done(scrumMaster);

            // Assert
            Assert.IsType<ToDoState>(item.State);
        }

        [Fact]
        public void TC0207GoingThroughStatesInTheRightWayToReadyForTesting()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);

            // Assert
            Assert.IsType<ReadyForTestingState>(item.State);
        }

        [Fact]
        public void TC0208GoingThroughStatesInTheRightWayToReadyForTestingButNotByCorrectUser()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(scrumMaster);

            // Assert
            Assert.IsType<DoingState>(item.State);
        }

        [Fact]
        public void TC0209GoingThroughStatesInTheRightWayToReadyForTestingAndBackToToDoState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.ToDo(tester);

            // Assert
            Assert.IsType<ToDoState>(item.State);
        }

        [Fact]
        public void TC0210GoingThroughStatesInTheRightWayToReadyForTestingAndBackToToDoStateButNotByCorrectUser()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.ToDo(scrumMaster);

            // Assert
            Assert.IsType<ReadyForTestingState>(item.State);
        }

        [Fact]
        public void TC0211CanNotGoFromReadyForTestingBackToDoingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Doing();

            // Assert
            Assert.IsType<ReadyForTestingState>(item.State);
        }

        [Fact]
        public void TC0212GoingThroughStatesInTheRightWayToTesting()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();

            // Assert
            Assert.IsType<TestingState>(item.State);
        }

        [Fact]
        public void TC0213CanNotGoFromReadyForTestingToTestedState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Tested();

            // Assert
            Assert.IsType<ReadyForTestingState>(item.State);
        }

        [Fact]
        public void TC0214CanNotGoFromReadyForTestingToDoneState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Done(scrumMaster);

            // Assert
            Assert.IsType<ReadyForTestingState>(item.State);
        }

        [Fact]
        public void TC0215CanNotGoFromTestingToToDoState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.ToDo(tester);

            // Assert
            Assert.IsType<TestingState>(item.State);
        }

        [Fact]
        public void TC0216CanNotGoFromTestingToDoingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Doing();

            // Assert
            Assert.IsType<TestingState>(item.State);
        }

        [Fact]
        public void TC0217CanNotGoFromTestingToReadyForTestingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.ReadyForTesting(tester);

            // Assert
            Assert.IsType<TestingState>(item.State);
        }

        [Fact]
        public void TC0218CanNotGoFromTestingToDoneState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Done(scrumMaster);

            // Assert
            Assert.IsType<TestingState>(item.State);
        }

        [Fact]
        public void TC0219GoingThroughStatesInTheRightWayToTested()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();

            // Assert
            Assert.IsType<TestedState>(item.State);
        }
        
        [Fact]
        public void TC0220CanNotGoFromTestedToToDoState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.ToDo(scrumMaster);

            // Assert
            Assert.IsType<TestedState>(item.State);
        }

        [Fact]
        public void TC0221CanNotGoFromTestedToDoingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.Doing();

            // Assert
            Assert.IsType<TestedState>(item.State);
        }

        [Fact]
        public void TC0222GoingThroughStatesInTheRightWayToTestedAndBackToReadyForTestingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.ReadyForTesting(scrumMaster);

            // Assert
            Assert.IsType<ReadyForTestingState>(item.State);
        }

        [Fact]
        public void TC0223GoingThroughStatesInTheRightWayToTestedAndBackToReadyForTestingStateButNotByCorrectUser()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.ReadyForTesting(developer);

            // Assert
            Assert.IsType<TestedState>(item.State);
        }

        [Fact]
        public void TC0224CanNotGoFromTestedToTestingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.Testing();

            // Assert
            Assert.IsType<TestedState>(item.State);
        }

        [Fact]
        public void TC0225GoingThroughStatesInTheRightWayToDone()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.Done(scrumMaster);

            // Assert
            Assert.IsType<DoneState>(item.State);
        }

        [Fact]
        public void TC0226GoingThroughStatesInTheRightWayToDoneButNotByCorrectUser()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.Done(developer);

            // Assert
            Assert.IsType<TestedState>(item.State);
        }

        [Fact]
        public void TC0227GoingThroughStatesInTheRightWayToDoneAndBackToToDoState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.Done(scrumMaster);
            item.State.ToDo(scrumMaster);

            // Assert
            Assert.IsType<ToDoState>(item.State);
        }

        [Fact]
        public void TC0228GoingThroughStatesInTheRightWayToDoneAndBackToToDoStateButNotByCorrectUser()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.Done(scrumMaster);
            item.State.ToDo(productOwner);

            // Assert
            Assert.IsType<DoneState>(item.State);
        }

        [Fact]
        public void TC0229CanNotGoFromDoneToDoingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.Done(scrumMaster);
            item.State.Doing();

            // Assert
            Assert.IsType<DoneState>(item.State);
        }

        [Fact]
        public void TC0230CanNotGoFromDoneToReadyForTestingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.Done(scrumMaster);
            item.State.ReadyForTesting(scrumMaster);

            // Assert
            Assert.IsType<DoneState>(item.State);
        }

        [Fact]
        public void TC0231CanNotGoFromDoneToTestingState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.Done(scrumMaster);
            item.State.Testing();

            // Assert
            Assert.IsType<DoneState>(item.State);
        }

        [Fact]
        public void TC0232CanNotGoFromDoneToTestedState()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 2);

            // Act
            item.State.Doing();
            item.State.ReadyForTesting(developer);
            item.State.Testing();
            item.State.Tested();
            item.State.Done(scrumMaster);
            item.State.Tested();

            // Assert
            Assert.IsType<DoneState>(item.State);
        }
    }
}
