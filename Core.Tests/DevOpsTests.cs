using System;
using System.Collections.Generic;
using Core.Domain.DevOps;
using Core.Domain.DevOps.Components;
using Core.Domain.DevOps.Components.Analyse;
using Core.Domain.DevOps.Components.Build;
using Core.Domain.DevOps.Components.Deploy;
using Core.Domain.DevOps.Components.Package;
using Core.Domain.DevOps.Components.Sources;
using Core.Domain.DevOps.Components.Test;
using Core.Domain.DevOps.Components.Utility;
using Core.Domain.DevOps.Visitors;
using Xunit;

namespace Core.Tests;

public class DevOpsTests
{
    private const string DUMMY_STRING = "DUMMY";

    [Fact]
    public void TC1101_CountPipelineOperationsCorrectly()
    {
        //Arrange
        var job1 = new Job(DUMMY_STRING);
        job1.Add(new CopyFile(DUMMY_STRING, DUMMY_STRING));
        job1.Add(new MoveFile(DUMMY_STRING,DUMMY_STRING));
        job1.Add(new DeleteFile(DUMMY_STRING));
        var job2 = new Job(DUMMY_STRING);
        job2.Add(new CopyFile(DUMMY_STRING, DUMMY_STRING));
        job2.Add(new DeleteFile(DUMMY_STRING));
        
        var devPipeline = new DevelopmentPipeline(DUMMY_STRING);
        devPipeline.addJob(job1);
        devPipeline.addJob(job2);
        var operationCountVisitor = new OperationCountVisitor();

        //Act
        devPipeline.accept(operationCountVisitor);

        //Assert
        Assert.Equal(operationCountVisitor.JobCount, 2);
        Assert.Equal(operationCountVisitor.OperationCount, 5);
    }

    [Fact]
    public void TC1102_RemoveOperation()
    {
        //Arrange
        var job = new Job(DUMMY_STRING);
        job.Add(new CopyFile(DUMMY_STRING, DUMMY_STRING));
        job.Add(new MoveFile(DUMMY_STRING,DUMMY_STRING));
        var deleteOperation = new DeleteFile(DUMMY_STRING);
        job.Add(deleteOperation);
        
        var devPipeline = new DevelopmentPipeline(DUMMY_STRING);
        devPipeline.addJob(job);
        var operationCountVisitor = new OperationCountVisitor();
        
        //ActAndAssert
        devPipeline.accept(operationCountVisitor);
        Assert.Equal(operationCountVisitor.OperationCount, 3);
        job.Remove(deleteOperation);
        operationCountVisitor.ResetCounter();
        devPipeline.accept(operationCountVisitor);
        Assert.Equal(operationCountVisitor.OperationCount, 2);
    }

    [Fact]
    public void TC1103_RemoveJob()
    {
        //Arrange
        var job1 = GetSampleJob();
        var job2 = GetSampleJob();
        
        var devPipeline = new DevelopmentPipeline(DUMMY_STRING);
        devPipeline.addJob(job1);
        devPipeline.addJob(job2);
        var operationCountVisitor = new OperationCountVisitor();

        //ActAndAssert
        devPipeline.accept(operationCountVisitor);
        Assert.Equal(operationCountVisitor.JobCount, 2);
        devPipeline.removeJob(job2);
        operationCountVisitor.ResetCounter();
        devPipeline.accept(operationCountVisitor);
        Assert.Equal(operationCountVisitor.JobCount, 1);
    }

    [Fact]
    public void TC1104_JobsCannotBeChildrenOfJobs()
    {
        //Arrange
        var job1 = GetSampleJob();
        var job2 = GetSampleJob();
        
        //ActAndAssert
        Assert.Throws<InvalidOperationException>(() => job1.Add(job2));
    }
    
    
    //Assisting functions
    private Job GetSampleJob()
    {
        var result = new Job(DUMMY_STRING);
        result.Add(new CopyFile(DUMMY_STRING, DUMMY_STRING));
        result.Add(new DeleteFile(DUMMY_STRING));
        result.Add(new MoveFile(DUMMY_STRING, DUMMY_STRING));
        result.Add(new ChangeDirectory(DUMMY_STRING));
        result.Add(new Script());
        result.Add(new GitClone(DUMMY_STRING));
        result.Add(new GitCheckout(DUMMY_STRING));
        result.Add(new GitPull());
        result.Add(new GitPull(DUMMY_STRING));
        result.Add(new GitPull(DUMMY_STRING, DUMMY_STRING));
        result.Add(new DotNetTest());
        result.Add(new DotNetTest(true, DUMMY_STRING));
        result.Add(new MochaTest(DUMMY_STRING));
        result.Add(new NUnitTest(DUMMY_STRING));
        result.Add(new AptInstall(new List<string>()));
        result.Add(new NpmInstall());
        result.Add(new NpmInstall(new List<string>()));
        result.Add(new NuGetInstall());
        result.Add(new NuGetInstall(new List<string>()));
        result.Add(new AwsDeploy(DUMMY_STRING, DUMMY_STRING));
        result.Add(new AzureDeploy(DUMMY_STRING, DUMMY_STRING));
        result.Add(new HerokuDeploy(DUMMY_STRING, DUMMY_STRING));
        result.Add(new DotNetBuild());
        result.Add(new DotNetBuild(DUMMY_STRING));
        result.Add(new MavenBuild());
        result.Add(new MavenBuild(DUMMY_STRING));
        result.Add(new RustBuild());
        result.Add(new RustBuild(DUMMY_STRING));
        result.Add(new SonarScannerBegin(DUMMY_STRING, DUMMY_STRING, DUMMY_STRING));
        result.Add(new SonarScannerEnd(DUMMY_STRING));
        return result;
    }
}