﻿using Core.Domain.Forum;
using Core.Domain.ProductBacklogItems;
using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Core.Tests
{
    public class MessageTests
    {
        [Fact]
        public void TC0901CreateThread()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 1);
            Developer user = new Developer("Arno", "Broeders", "a.broeders@avans.nl");
            Message message = new Message("Ik heb een suggestie.", user);

            // Act
            item.CreateThread(message);

            // Assert
            Assert.NotNull(item.Thread);
            Assert.Single(item.Thread.Messages);
        }

        [Fact]
        public void TC0902AddMultipleMessages()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 1);
            Developer user = new Developer("Arno", "Broeders", "a.broeders@avans.nl");
            Message message1 = new Message("Ik heb een suggestie.", user);
            Message message2 = new Message("Er kan hier een pattern worden toegepast.", user);

            // Act
            item.CreateThread(message1);
            item.AddMessageToThread(message2);

            // Assert
            Assert.Equal(2, item.Thread.Messages.Count);
        }

        [Fact]
        public void TC0903CannotAddMessageToThread()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 1);
            Developer user = new Developer("Arno", "Broeders", "a.broeders@avans.nl");
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");
            Message message1 = new Message("Ik heb een suggestie.", user);
            Message message2 = new Message("Er kan hier een pattern worden toegepast.", user);
            Message message3 = new Message("Er kan hier een pattern worden toegepast.", scrumMaster);

            // Act
            item.CreateThread(message1);
            item.State.Doing();
            item.State.ReadyForTesting(user);
            item.AddMessageToThread(message2);
            item.State.Testing();
            item.State.Tested();
            item.State.Done(scrumMaster);
            item.AddMessageToThread(message3);

            // Assert
            Assert.Equal(2, item.Thread.Messages.Count);
        }

        [Fact]
        public void TC0904AddMessageWithoutAThread()
        {
            // Arrange
            ProductBacklogItem item = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 1);
            Developer user = new Developer("Arno", "Broeders", "a.broeders@avans.nl");
            Message message1 = new Message("Ik heb een suggestie.", user);

            // Act
            item.AddMessageToThread(message1);

            // Assert
            Assert.Null(item.Thread);
        }
    }
}
