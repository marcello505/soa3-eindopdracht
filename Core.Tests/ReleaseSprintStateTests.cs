﻿using Core.Domain;
using Core.Domain.DevOps;
using Core.Domain.DevOps.Components;
using Core.Domain.DevOps.Components.Build;
using Core.Domain.DevOps.Components.Debug;
using Core.Domain.DevOps.Components.Deploy;
using Core.Domain.DevOps.Components.Package;
using Core.Domain.DevOps.Components.Sources;
using Core.Domain.DevOps.Components.Test;
using Core.Domain.ProductBacklogItems;
using Core.Domain.TypesOfSprints.ReleaseSprints;
using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Core.Tests
{
    public class ReleaseSprintStateTests
    {
        private const string DUMMY_STRING = "DUMMY";

        [Fact]
        public void TC0401NewReleaseSprintHasSprintNumber()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);

            // Assert
            Assert.Equal(1, sprint.SprintNumber);
        }

        [Fact]
        public void TC0402NewReleaseSprintHasStartAndEndDate()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);

            // Assert
            Assert.Equal(new DateTime(2022, 03, 22), sprint.StartDate);
            Assert.Equal(new DateTime(2022, 04, 05), sprint.EndDate);
        }

        [Fact]
        public void TC0403NewReleaseSprintHasEmptyItemList()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);

            // Assert
            Assert.Empty(sprint.Items);
        }

        [Fact]
        public void TC0404NewReleaseSprintHasEmptyTeamList()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);

            // Assert
            Assert.Empty(sprint.Team);
        }

        [Fact]
        public void TC0405NewReleaseSprintHasCreatedState()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);

            // Assert
            Assert.IsType<CreatedState>(sprint.State);
        }

        [Fact]
        public void TC0501ReleaseSprintToStateInProgress()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            sprint.State.InProgress(scrumMaster);

            // Assert
            Assert.IsType<InProgressState>(sprint.State);
        }

        [Fact]
        public void TC0502ReleaseSprintNotToStateInProgress()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            Developer developer = new Developer("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            sprint.State.InProgress(developer);

            // Assert
            Assert.IsType<CreatedState>(sprint.State);
        }

        [Fact]
        public void TC0503CannotUpdateSprintNumberInInProgressState()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            sprint.State.InProgress(scrumMaster);
            sprint.SetSprintNumber(2);

            // Assert
            Assert.Equal(1, sprint.SprintNumber);
        }

        [Fact]
        public void TC0504CannotUpdateStartOrEndDateInInProgressState()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            sprint.State.InProgress(scrumMaster);
            sprint.SetStartDate(new DateTime(2022, 03, 23));
            sprint.SetEndDate(new DateTime(2022, 04, 06));

            // Assert
            Assert.Equal(startDate, sprint.StartDate);
            Assert.Equal(endDate, sprint.EndDate);
        }

        [Fact]
        public void TC0601AddMemberToReleaseSprintAndCheckProductOwner()
        {
            // Arrange
            Project project = new Project("SOFA3");
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ProductOwner productOwner = new ProductOwner("Arno", "Broeders", "a.broeders@avans.nl");          
            Developer developer = new Developer("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            project.AssignProductOwner(productOwner);
            project.CreateNewReleaseSprint(1, startDate, endDate);
            project.SprintBacklogs[0].AddTeamMember(developer);

            // Assert
            Assert.NotNull(project.ProductOwner);
            Assert.NotNull(project.SprintBacklogs[0].Team);
        }

        [Fact]
        public void TC0602AddMultipleMembersToReleaseSprintAndCheckProductOwner()
        {
            // Arrange
            Project project = new Project("SOFA3");
            var startDate = new DateTime(2022, 03, 22);
            var endDate = new DateTime(2022, 04, 05);
            ProductOwner productOwner = new ProductOwner("Arno", "Broeders", "a.broeders@avans.nl");
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");
            Developer developer = new Developer("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            project.AssignProductOwner(productOwner);
            project.CreateNewReleaseSprint(1, startDate, endDate);
            project.SprintBacklogs[0].AddTeamMember(scrumMaster);
            project.SprintBacklogs[0].AddTeamMember(developer);

            // Assert
            Assert.NotNull(project.ProductOwner);
            Assert.Equal(2, project.SprintBacklogs[0].Team.Count);
        }

        [Fact]
        public void TC0701ReleaseSprintToFinishedState()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 11);
            var endDate = new DateTime(2022, 03, 25);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            sprint.State.InProgress(scrumMaster);
            sprint.State.Finished(DateTime.Now);

            // Assert
            Assert.IsType<FinishedState>(sprint.State);
        }

        [Fact]
        public void TC0702ReleaseSprintNotToFinishedStateBecauseToEarly()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 11);
            var endDate = new DateTime(2022, 03, 25);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            sprint.State.InProgress(scrumMaster);
            sprint.State.Finished(new DateTime(2022, 03, 23));

            // Assert
            Assert.IsType<InProgressState>(sprint.State);
        }

        [Fact]
        public void TC0703ReleaseSprintToFinishedStateCanNotAddProductBacklogItem()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 11);
            var endDate = new DateTime(2022, 03, 25);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");
            ProductBacklogItem item1 = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 1);
            ProductBacklogItem item2 = new ProductBacklogItem("Als gebruiker wil ik een fase kunnen toekennen.", 1);

            // Act
            sprint.State.InProgress(scrumMaster);
            sprint.AddItem(item1);
            sprint.State.Finished(DateTime.Now);
            sprint.AddItem(item2);

            // Assert
            Assert.Single(sprint.Items);
        }

        [Fact]
        public void TC0801StartReleaseDevelopmentPipelineToCompletedState()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 11);
            var endDate = new DateTime(2022, 03, 25);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");
            DevelopmentPipeline devPipeline = new DevelopmentPipeline(DUMMY_STRING);
            var sources = new Job(DUMMY_STRING);
            sources.Add(new GitCheckout(DUMMY_STRING));
            sources.Add(new GitPull());
            var package = new Job(DUMMY_STRING);
            package.Add(new NpmInstall());
            var build = new Job(DUMMY_STRING);
            build.Add(new DotNetBuild());
            var test = new Job(DUMMY_STRING);
            test.Add(new DotNetTest());
            var deploy = new Job(DUMMY_STRING);
            deploy.Add(new AzureDeploy(DUMMY_STRING, DUMMY_STRING));

            // Act            
            devPipeline.addJob(sources);
            devPipeline.addJob(package);
            devPipeline.addJob(build);
            devPipeline.addJob(test);
            devPipeline.addJob(deploy);
            sprint.SetPipeline(devPipeline);

            sprint.State.InProgress(scrumMaster);
            sprint.State.Finished(DateTime.Now);
            sprint.State.Release();

            // Assert
            Assert.IsType<CompletedState>(sprint.State);
        }

        [Fact]
        public void TC0802StartReleaseDevelopmentPipelineToCancelledStateThereIsNoPipeline()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 11);
            var endDate = new DateTime(2022, 03, 25);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            sprint.State.InProgress(scrumMaster);
            sprint.State.Finished(DateTime.Now);
            sprint.State.Release();

            // Assert
            Assert.IsType<CancelledState>(sprint.State);
        }

        [Fact]
        public void TC0803StartReleaseDevelopmentPipelineToCancelledStateThereIsAnError()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 11);
            var endDate = new DateTime(2022, 03, 25);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");
            DevelopmentPipeline devPipeline = new DevelopmentPipeline(DUMMY_STRING);
            var sources = new Job(DUMMY_STRING);
            sources.Add(new GitCheckout(DUMMY_STRING));
            sources.Add(new GitPull());
            var package = new Job(DUMMY_STRING);
            package.Add(new NpmInstall());
            var build = new Job(DUMMY_STRING);
            build.Add(new DotNetBuild());
            var test = new Job(DUMMY_STRING);
            test.Add(new Crash());
            var deploy = new Job(DUMMY_STRING);
            deploy.Add(new AzureDeploy(DUMMY_STRING, DUMMY_STRING));

            // Act            
            devPipeline.addJob(sources);
            devPipeline.addJob(package);
            devPipeline.addJob(build);
            devPipeline.addJob(test);
            devPipeline.addJob(deploy);
            sprint.SetPipeline(devPipeline);

            sprint.State.InProgress(scrumMaster);
            sprint.State.Finished(DateTime.Now);
            sprint.State.Release();

            // Assert
            Assert.IsType<CancelledState>(sprint.State);
        }

        [Fact]
        public void TC0804StartReleaseFromCancelledState()
        {
            // Arrange
            var startDate = new DateTime(2022, 03, 11);
            var endDate = new DateTime(2022, 03, 25);
            ReleaseSprint sprint = new ReleaseSprint(1, startDate, endDate);
            ScrumMaster scrumMaster = new ScrumMaster("Arno", "Broeders", "a.broeders@avans.nl");

            // Act
            sprint.State.InProgress(scrumMaster);
            sprint.State.Finished(DateTime.Now);
            sprint.State.Release();

            // Arrange more
            DevelopmentPipeline devPipeline = new DevelopmentPipeline(DUMMY_STRING);
            var sources = new Job(DUMMY_STRING);
            sources.Add(new GitCheckout(DUMMY_STRING));
            sources.Add(new GitPull());
            var package = new Job(DUMMY_STRING);
            package.Add(new NpmInstall());
            var build = new Job(DUMMY_STRING);
            build.Add(new DotNetBuild());
            var test = new Job(DUMMY_STRING);
            test.Add(new DotNetTest());
            var deploy = new Job(DUMMY_STRING);
            deploy.Add(new AzureDeploy(DUMMY_STRING, DUMMY_STRING));

            // Act           
            devPipeline.addJob(sources);
            devPipeline.addJob(package);
            devPipeline.addJob(build);
            devPipeline.addJob(test);
            devPipeline.addJob(deploy);
            sprint.SetPipeline(devPipeline);

            sprint.State.Release();

            // Assert
            Assert.IsType<CompletedState>(sprint.State);
        }
    }
}
