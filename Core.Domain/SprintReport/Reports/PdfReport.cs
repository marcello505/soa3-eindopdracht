namespace Core.Domain.SprintReport.Reports;

public class PdfReport
{
    public string HeaderContent = "";
    public string BodyContent = "";
    public string FooterContent = "";
}