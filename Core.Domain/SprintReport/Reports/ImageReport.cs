namespace Core.Domain.SprintReport.Reports;

public class ImageReport
{
    public byte[] Image;
    public Extensions Extension;

    public enum Extensions
    {
        Png,
        Jpg
    }

    public ImageReport(byte[] image, Extensions extension)
    {
        Image = image;
        Extension = extension;
    }
}