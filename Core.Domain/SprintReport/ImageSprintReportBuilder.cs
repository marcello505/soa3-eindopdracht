using System.Text;
using System.Xml.Serialization;
using Core.Domain.SprintReport.Reports;

namespace Core.Domain.SprintReport;

public class ImageSprintReportBuilder: AbstractSprintReportBuilder
{
    public ImageSprintReportBuilder(SprintBacklog parentSprintBacklog) : base(parentSprintBacklog)
    {
    }

    public ImageReport GetResult(ImageReport.Extensions extension)
    {
        const byte dummyByte = 1;
        var imageStream = new MemoryStream();
        //Body
        if (TeamComposition != null) imageStream.WriteByte(dummyByte);
        if (BurnDownChart) imageStream.WriteByte(dummyByte);
        if (EffortPointOverview != null) imageStream.WriteByte(dummyByte);
        //Header
        if (HeaderLogo != null) imageStream.WriteByte(dummyByte);
        if (HeaderProjectName != null) imageStream.WriteByte(dummyByte);
        if (HeaderVersion != null) imageStream.WriteByte(dummyByte);
        if (HeaderDate.HasValue) imageStream.WriteByte(dummyByte);
        //Footer
        if (FooterLogo != null) imageStream.WriteByte(dummyByte);
        if (FooterProjectName != null) imageStream.WriteByte(dummyByte);
        if (FooterVersion != null) imageStream.WriteByte(dummyByte);
        if (FooterDate.HasValue) imageStream.WriteByte(dummyByte);

        var result = new ImageReport(imageStream.ToArray(), extension);
        imageStream.Close();
        return result;
    }
}