using System.Text;
using System.Xml.Serialization;
using Core.Domain.SprintReport.Reports;

namespace Core.Domain.SprintReport;

public class PdfSprintReportBuilder: AbstractSprintReportBuilder
{
    public PdfSprintReportBuilder(SprintBacklog parentSprintBacklog) : base(parentSprintBacklog)
    {
    }
    
    public PdfReport GetResult()
    {
        var result = new PdfReport();
        var bodyContentSb = new StringBuilder();
        //teamcomp
        if (TeamComposition != null)
        {
            bodyContentSb.AppendLine("<team>");
            foreach (var user in TeamComposition)
            {
                bodyContentSb.AppendFormat("{0} {1} <{2}> \n", user.FirstName, user.LastName, user.Email);
            }

            bodyContentSb.AppendLine("</team>");
        }
        
        //burndown
        if (BurnDownChart) bodyContentSb.AppendLine("<burndownchart />");

        if (EffortPointOverview != null)
        {
            bodyContentSb.AppendLine("<effortpointoverview>");
            foreach (var item in EffortPointOverview)
            {
                bodyContentSb.AppendFormat("{0} - {1} points\n", item.Key.FirstName, item.Value);
            }
            bodyContentSb.AppendLine("</effortpointoverview>");
        }
        
        //Header content
        var headerSb = new StringBuilder();
        if (HeaderLogo != null) headerSb.AppendFormat("<img src={0}/> ", Convert.ToBase64String(HeaderLogo));
        if (HeaderProjectName != null) headerSb.AppendFormat("<h1>{0}</h1>", HeaderProjectName);
        if (HeaderVersion != null) headerSb.AppendFormat("<span id=\"version\">{0}</span>", HeaderVersion);
        if (HeaderDate.HasValue) headerSb.AppendFormat("<span id=\"date\">{0}</span>", HeaderDate.Value.ToString());
        
        //Footer content
        var footerSb = new StringBuilder();
        if (FooterLogo != null) footerSb.AppendFormat("<img src={0}/> ", Convert.ToBase64String(FooterLogo));
        if (FooterProjectName != null) footerSb.AppendFormat("<h1>{0}</h1>", FooterProjectName);
        if (FooterVersion != null) footerSb.AppendFormat("<span id=\"version\">{0}</span>", FooterVersion);
        if (FooterDate.HasValue) footerSb.AppendFormat("<span id=\"date\">{0}</span>", FooterDate.Value.ToString());

        result.BodyContent = bodyContentSb.ToString();
        result.HeaderContent = headerSb.ToString();
        result.FooterContent = footerSb.ToString();

        return result;
    }

}