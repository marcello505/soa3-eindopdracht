using System.Text;
using System.Xml.Serialization;
using Core.Domain.SprintReport.Reports;

namespace Core.Domain.SprintReport;

public abstract class AbstractSprintReportBuilder
{
    private SprintBacklog _parentSprintBacklog;

    protected AbstractSprintReportBuilder(SprintBacklog parentSprintBacklog)
    {
        _parentSprintBacklog = parentSprintBacklog;
    }


    internal IList<User>? TeamComposition = null;
    internal bool BurnDownChart = false;
    internal IDictionary<User, int>? EffortPointOverview = null;
    internal byte[]? HeaderLogo = null;
    internal string? HeaderProjectName = null;
    internal string? HeaderVersion = null;
    internal DateTime? HeaderDate = null;
    internal byte[]? FooterLogo = null;
    internal string? FooterProjectName = null;
    internal string? FooterVersion = null;
    internal DateTime? FooterDate = null;

    //Methods
    public void Reset()
    {
        TeamComposition = null;
        BurnDownChart = false;
        EffortPointOverview = null;
        HeaderLogo = null;
        HeaderProjectName = null;
        HeaderVersion = null;
        HeaderDate = null;
        FooterLogo = null;
        FooterProjectName = null;
        FooterVersion = null;
        FooterDate = null;
    }

    public void AddTeamComposition()
    {
        TeamComposition = new List<User>();

        foreach (var user in _parentSprintBacklog.Team)
        {
            TeamComposition.Add(user);
        }
    }

    public void AddBurnDownChart()
    {
        BurnDownChart = true;
    }

    public void AddEffortPointOverview()
    {
        EffortPointOverview = new Dictionary<User, int>();
        foreach (var backlogItem in _parentSprintBacklog.Items)
        {
            if (backlogItem.State is ProductBacklogItems.DoneState && backlogItem.Developer != null)
            {
                //backlogitem is finished
                if (EffortPointOverview.ContainsKey(backlogItem.Developer))
                {
                    //Developer is already inside the dictionary
                    EffortPointOverview[backlogItem.Developer] += backlogItem.Effort;
                }
                else
                {
                    //Developer isn't inside the dictionary
                    EffortPointOverview.Add(backlogItem.Developer, backlogItem.Effort);
                }
            }
        }
    }

    public void SetHeaderLogo(byte[] image)
    {
        HeaderLogo = image;
    }

    public void SetHeaderProjectName(string projectName)
    {
        HeaderProjectName = projectName;
    }

    public void SetHeaderVersion(string version)
    {
        HeaderVersion = version;
    }

    public void SetHeaderDate(DateTime dateTime)
    {
        HeaderDate = dateTime;
    }

    public void SetFooterLogo(byte[] image)
    {
        FooterLogo = image;
    }

    public void SetFooterProjectName(string projectName)
    {
        FooterProjectName = projectName;
    }

    public void SetFooterVersion(string version)
    {
        FooterVersion = version;
    }

    public void SetFooterDate(DateTime dateTime)
    {
        FooterDate = dateTime;
    }
}