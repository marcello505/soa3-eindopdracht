namespace Core.Domain.Activities.States;

public class TodoState: IActivityState
{
    public TodoState(Activity parentActivity)
    {
        ParentActivity = parentActivity;
    }

    public Activity ParentActivity { get; set; }

    public void Todo()
    {
        throw new InvalidOperationException();
    }

    public void Doing()
    {
        if (ParentActivity.Developer != null)
        {
            ParentActivity.State = new DoingState(ParentActivity);
        }
        else
        {
            throw new InvalidOperationException("Can only switch state to \"Doing\" if a developer has been assigned.");
        }
    }

    public void Done()
    {
        throw new InvalidOperationException();
    }
}