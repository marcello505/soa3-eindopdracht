namespace Core.Domain.Activities.States;

public interface IActivityState
{
    public Activity ParentActivity { get; set; }
    void Todo();
    void Doing();
    void Done();
}