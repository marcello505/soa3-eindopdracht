namespace Core.Domain.Activities.States;

public class DoingState: IActivityState
{
    public DoingState(Activity parentActivity)
    {
        ParentActivity = parentActivity;
    }

    public Activity ParentActivity { get; set; }

    public void Todo()
    {
        throw new InvalidOperationException();
    }

    public void Doing()
    {
        throw new InvalidOperationException();
    }

    public void Done()
    {
        ParentActivity.State = new DoneState(ParentActivity);
    }
}