namespace Core.Domain.Activities.States;

public class DoneState: IActivityState
{
    public DoneState(Activity parentActivity)
    {
        ParentActivity = parentActivity;
    }

    public Activity ParentActivity { get; set; }
    public void Todo()
    {
        ParentActivity.State = new TodoState(ParentActivity);
    }

    public void Doing()
    {
        throw new InvalidOperationException();
    }

    public void Done()
    {
        throw new InvalidOperationException();
    }
}