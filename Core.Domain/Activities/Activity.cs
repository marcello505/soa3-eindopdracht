﻿using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.Activities.States;
using Core.Domain.ProductBacklogItems;

namespace Core.Domain.Activities
{
    public class Activity
    {
        public string Task { get; set; }
        public ProductBacklogItem ParentProductBacklogItem { get; set; }
        public Developer? Developer { get; set; }
        public IActivityState State { get; set; }

        public Activity(string task)
        {
            Task = task;
            State = new TodoState(this);
        }
    }
}
