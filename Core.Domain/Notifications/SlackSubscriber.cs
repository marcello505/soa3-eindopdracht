namespace Core.Domain.Notifications;

public class SlackSubscriber: INotificationSubscriber
{
    private string SlackUsername;
    public bool NotificationReceived { get; set; } = false;

    public SlackSubscriber(string slackUsername)
    {
        SlackUsername = slackUsername;
    }
    
    public void Notify()
    {
        NotificationReceived = true;
    }
}