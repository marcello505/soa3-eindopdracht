namespace Core.Domain.Notifications;

public class EmailSubscriber: INotificationSubscriber
{
    private string EmailAddress;
    public bool NotificationReceived { get; set; } = false;

    public EmailSubscriber(string emailAddress)
    {
        EmailAddress = emailAddress;
    }


    public void Notify()
    {
        NotificationReceived = true;
    }
}