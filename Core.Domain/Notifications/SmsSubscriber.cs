namespace Core.Domain.Notifications;

public class SmsSubscriber: INotificationSubscriber
{
    private string PhoneNumber;
    public bool NotificationReceived { get; set; } = false;

    public SmsSubscriber(string phoneNumber)
    {
        PhoneNumber = phoneNumber;
    }
    
    public void Notify()
    {
        NotificationReceived = true;
    }
}