namespace Core.Domain.Notifications;

public interface INotificationSubscriber
{
    public bool NotificationReceived { get; set; }
    void Notify();
}