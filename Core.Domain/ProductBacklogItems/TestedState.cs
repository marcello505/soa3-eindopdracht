﻿using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.ProductBacklogItems
{
    public class TestedState : ItemState
    {
        public TestedState(ProductBacklogItem item) : base(item)
        {
        }

        public override void Done(User user)
        {
            if (user.GetType() == typeof(ScrumMaster) && (Item.Activities.Count == 0 || Item.Activities.All(a => a.State is Activities.States.DoneState)))
            {
                Item.State = new DoneState(Item);
            }
        }

        public override void ReadyForTesting(User user)
        {
            if (user.GetType() == typeof(ScrumMaster))
            {
                NotifyTesters();
                Item.State = new ReadyForTestingState(Item);
            }
        }

        private void NotifyTesters()
        {
            if (Item.SprintBacklog != null)
            {
                foreach (var item in Item.SprintBacklog.Team)
                {
                    if (item is Tester)
                    {
                        item.Notify();
                    }
                }
            }
        }
    }
}
        

