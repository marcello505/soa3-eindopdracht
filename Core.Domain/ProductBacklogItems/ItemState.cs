﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.ProductBacklogItems
{
    public abstract class ItemState
    {
        public ProductBacklogItem Item { get; set; }

        protected ItemState(ProductBacklogItem item)
        {
            Item = item;
        }

        public virtual void ToDo(User user) { }
        public virtual void Doing() { }
        public virtual void ReadyForTesting(User user) { }
        public virtual void Testing() { }
        public virtual void Tested() { }
        public virtual void Done(User user) { }
    }
}
