﻿using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.ProductBacklogItems
{
    public class DoingState : ItemState
    {
        public DoingState(ProductBacklogItem item) : base(item)
        {
        }

        public override void ReadyForTesting(User user)
        {
            if (user.GetType() == typeof(Developer))
            {
                NotifyTesters();
                Item.State = new ReadyForTestingState(Item);
            }                
        }

        private void NotifyTesters()
        {
            if (Item.SprintBacklog != null)
            {
                foreach (var item in Item.SprintBacklog.Team)
                {
                    if (item is Tester)
                    {
                        item.Notify();
                    }
                }
            }
        }
    }
}