﻿using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.ProductBacklogItems
{
    public class ReadyForTestingState : ItemState
    {
        public ReadyForTestingState(ProductBacklogItem item) : base(item)
        {
        }

        public override void Testing()
        {
            Item.State = new TestingState(Item);
        }

        public override void ToDo(User user)
        {
            if (user.GetType() == typeof(Tester))
            {
                NotifyScrumMasters();
                Item.State = new ToDoState(Item);
            }               
        }
        private void NotifyScrumMasters()
        {
            if (Item.SprintBacklog != null)
            {
                foreach (var item in Item.SprintBacklog.Team)
                {
                    if (item is ScrumMaster)
                    {
                        item.Notify();
                    }
                }
            }
        }
    }
}
        

