﻿using Core.Domain.Activities;
using Core.Domain.Forum;
using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.ProductBacklogItems
{
    public class ProductBacklogItem
    {
        public string UserStory { get; set; }
        public List<Activity> Activities { get; set; }
        public Developer? Developer { get; set; }
        public ItemState State { get; set; }
        public int Effort { get; set; }
        public SprintBacklog? SprintBacklog { get; set; }
        public Forum.Thread? Thread { get; set; }

        public ProductBacklogItem(string userStory, int effort)
        {
            UserStory = userStory;
            Activities = new List<Activity>();
            State = new ToDoState(this);
            Effort = effort;
        }

        public void AddActivity(Activity activity)
        {
            activity.ParentProductBacklogItem = this;
            Activities.Add(activity);
        }

        public void AssignDeveloper(Developer developer)
        {
            Developer = developer;
        }

        public void CreateThread(Message message)
        {
            Thread = new Forum.Thread(message);
        }

        public void AddMessageToThread(Message newMessage)
        {
            if(Thread != null && State.GetType() != typeof(DoneState))
            {
                Thread.AddMessage(newMessage);
            }            
        }
    }
}
