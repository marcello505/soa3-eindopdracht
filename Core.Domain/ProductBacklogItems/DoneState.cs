﻿using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.ProductBacklogItems
{
    public class DoneState : ItemState
    {
        public DoneState(ProductBacklogItem item) : base(item)
        {
        }

        public override void ToDo(User user)
        {
            if (user.GetType() == typeof(ScrumMaster) || user.GetType() == typeof(Tester))
            {
                Item.State = new ToDoState(Item);
            }                
        }
    }
}
