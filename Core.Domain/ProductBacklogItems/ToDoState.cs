﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.ProductBacklogItems
{
    public class ToDoState : ItemState
    {
        public ToDoState(ProductBacklogItem item) : base(item)
        {
        }

        public override void Doing()
        {
            Item.State = new DoingState(Item);
        }
    }
}
