﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.ProductBacklogItems
{
    public class TestingState : ItemState
    {
        public TestingState(ProductBacklogItem item) : base(item)
        {
        }

        public override void Tested()
        {
            Item.State = new TestedState(Item);
        }
    }
}
