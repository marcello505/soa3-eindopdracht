using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Test;

public class MochaTest: IComponent
{
    public string Path;

    public MochaTest(string path)
    {
        Path = path;
    }
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}