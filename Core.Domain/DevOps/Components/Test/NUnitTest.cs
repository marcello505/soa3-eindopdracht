using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Test;

public class NUnitTest: IComponent
{
    public string Path;

    public NUnitTest(string path)
    {
        Path = path;
    }
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}