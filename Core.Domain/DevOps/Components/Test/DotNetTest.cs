using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Test;

public class DotNetTest: IComponent
{
    public bool WithCoverage;
    public string CoverageReportFormat;

    public DotNetTest()
    {
        WithCoverage = false;
        CoverageReportFormat = "";
    }

    public DotNetTest(bool withCoverage, string coverageReportFormat)
    {
        WithCoverage = withCoverage;
        CoverageReportFormat = coverageReportFormat;
    }
    
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}