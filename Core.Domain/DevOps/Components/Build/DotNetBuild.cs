using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Build;

public class DotNetBuild : IComponent
{
    public string Path;

    public DotNetBuild()
    {
        Path = ".";
    }
    public DotNetBuild(string path)
    {
        Path = path;
    }
    
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}