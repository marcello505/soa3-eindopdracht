using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Build;

public class RustBuild: IComponent
{
    public string Path;

    public RustBuild()
    {
        Path = ".";
    }
    public RustBuild(string path)
    {
        Path = path;
    }
    
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}