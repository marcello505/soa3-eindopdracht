using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Build;

public class MavenBuild: IComponent
{
    public string Path;

    public MavenBuild()
    {
        Path = ".";
    }
    public MavenBuild(string path)
    {
        Path = path;
    }
    
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}