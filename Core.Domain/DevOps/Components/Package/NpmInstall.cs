using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Package;

public class NpmInstall: IComponent
{
    public IEnumerable<string> Packages;

    public NpmInstall()
    {
        Packages = Array.Empty<string>();
    }

    public NpmInstall(IEnumerable<string> packages)
    {
        Packages = packages;
    }
    
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}