using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Package;

public class AptInstall : IComponent
{
    public IEnumerable<string> Packages;
    public AptInstall(IEnumerable<string> packages)
    {
        Packages = packages;
    }

    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}