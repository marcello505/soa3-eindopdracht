using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Package;

public class NuGetInstall: IComponent
{
    public IEnumerable<string> Packages;

    public NuGetInstall()
    {
        Packages = Array.Empty<string>();
    }

    public NuGetInstall(IEnumerable<string> packages)
    {
        Packages = packages;
    }

    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}