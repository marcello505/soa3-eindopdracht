using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Debug;

public class Crash: IComponent
{
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}