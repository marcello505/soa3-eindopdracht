using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Utility;

public class ChangeDirectory : IComponent
{
    public string Path;
    public ChangeDirectory(string path)
    {
        Path = path;
    }
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}