using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Utility;

public class CopyFile : IComponent
{
    public string FileSourcePath;
    public string FileDestinationPath;

    public CopyFile(string fileSourcePath, string fileDestinationPath)
    {
        FileSourcePath = fileSourcePath;
        FileDestinationPath = fileDestinationPath;
    }

    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}