using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Utility;

public class Script: IComponent
{
    public IList<IComponent> Operations;

    public Script()
    {
        Operations = new List<IComponent>();
    }

    public void AddOperation(IComponent operation)
    {
        if (operation is Job) throw new InvalidOperationException("Can't add a Job as an operation");
        Operations.Add(operation);
    }

    public void RemoveOperation(IComponent operation)
    {
        Operations.Remove(operation);
    }

    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}