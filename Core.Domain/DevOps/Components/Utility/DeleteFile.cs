using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Utility;

public class DeleteFile : IComponent
{
    public string FileToDeletePath;

    public DeleteFile(string fileToDeletePath)
    {
        FileToDeletePath = fileToDeletePath;
    }

    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}