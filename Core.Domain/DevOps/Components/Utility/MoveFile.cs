using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Utility;

public class MoveFile : IComponent
{
    public string FileSourcePath;
    public string FileDestinationPath;

    public MoveFile(string fileSourcePath, string fileDestinationPath)
    {
        FileSourcePath = fileSourcePath;
        FileDestinationPath = fileDestinationPath;
    }

    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}