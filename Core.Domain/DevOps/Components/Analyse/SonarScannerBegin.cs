using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Analyse;

public class SonarScannerBegin: IComponent
{
    public string SonarKey;
    public string SonarRepo;
    public string SonarHostURL;

    public SonarScannerBegin(string sonarKey, string sonarRepo, string sonarHostUrl)
    {
        SonarKey = sonarKey;
        SonarRepo = sonarRepo;
        SonarHostURL = sonarHostUrl;
    }

    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}