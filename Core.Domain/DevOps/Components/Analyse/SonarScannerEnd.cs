using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Analyse;

public class SonarScannerEnd : IComponent
{
    public string SonarKey;
    public SonarScannerEnd(string sonarKey)
    {
        SonarKey = sonarKey;
    }

    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}