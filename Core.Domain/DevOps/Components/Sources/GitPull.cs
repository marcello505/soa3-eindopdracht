using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Sources;

public class GitPull: IComponent
{
    public string Repo;
    public string Branch;

    public GitPull()
    {
        Repo = "";
        Branch = "";
    }

    public GitPull(string repo)
    {
        Repo = repo;
        Branch = "";
    }

    public GitPull(string repo, string branch)
    {
        Repo = repo;
        Branch = branch;
    }
    
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}