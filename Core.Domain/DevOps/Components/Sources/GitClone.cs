using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Sources;

public class GitClone: IComponent
{
    public string GitURL;

    public GitClone(string gitUrl)
    {
        GitURL = gitUrl;
    }
    
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}