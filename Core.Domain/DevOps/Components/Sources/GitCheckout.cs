using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Sources;

public class GitCheckout: IComponent
{
    public string Branch;

    public GitCheckout(string branch)
    {
        Branch = branch;
    }
    
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}