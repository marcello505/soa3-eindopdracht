using System.Net.Sockets;
using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components;

public class Job : IComponent
{
    public readonly string Name;
    public IList<IComponent> Children;

    public Job(string name)
    {
        Name = name;
        Children = new List<IComponent>();
    }

    public void Add(IComponent component)
    {
        if (component is Job) throw new InvalidOperationException("Cannot add a Job Class as a child of a Job Class");
        Children.Add(component);
    }

    public void Remove(IComponent component)
    {
        Children.Remove(component);
    }

    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}