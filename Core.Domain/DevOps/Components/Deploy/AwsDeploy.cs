using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components.Deploy;

public class AwsDeploy: IComponent
{
    public string ServerName;
    public string DeployKey;

    public AwsDeploy(string serverName, string deployKey)
    {
        ServerName = serverName;
        DeployKey = deployKey;
    }
    public void Accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }
}