using Core.Domain.DevOps.Visitors;

namespace Core.Domain.DevOps.Components;

public interface IComponent
{
    public void Accept(IDevOpsVisitor visitor);

}