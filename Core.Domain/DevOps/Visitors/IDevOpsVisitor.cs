using Core.Domain.DevOps.Components;
using Core.Domain.DevOps.Components.Analyse;
using Core.Domain.DevOps.Components.Build;
using Core.Domain.DevOps.Components.Debug;
using Core.Domain.DevOps.Components.Deploy;
using Core.Domain.DevOps.Components.Package;
using Core.Domain.DevOps.Components.Sources;
using Core.Domain.DevOps.Components.Test;
using Core.Domain.DevOps.Components.Utility;

namespace Core.Domain.DevOps.Visitors;

public interface IDevOpsVisitor
{
    public void Visit(DevelopmentPipeline developmentPipeline);
    public void Visit(Job job);
    void Visit(CopyFile copyFile);
    void Visit(MoveFile moveFile);
    void Visit(DeleteFile deleteFile);
    void Visit(RustBuild rustBuild);
    void Visit(MavenBuild mavenBuild);
    void Visit(DotNetBuild dotNetBuild);
    void Visit(ChangeDirectory changeDirectory);
    void Visit(Script script);
    void Visit(NuGetInstall nuGetInstall);
    void Visit(NpmInstall npmInstall);
    void Visit(AptInstall aptInstall);
    void Visit(HerokuDeploy herokuDeploy);
    void Visit(AzureDeploy azureDeploy);
    void Visit(AwsDeploy awsDeploy);
    void Visit(SonarScannerBegin sonarScannerBegin);
    void Visit(SonarScannerEnd sonarScannerEnd);
    void Visit(GitPull gitPull);
    void Visit(GitCheckout gitCheckout);
    void Visit(GitClone gitClone);
    void Visit(DotNetTest dotNetTest);
    void Visit(MochaTest mochaTest);
    void Visit(NUnitTest nUnitTest);
    void Visit(Crash crash);
}