using Core.Domain.DevOps.Components;
using Core.Domain.DevOps.Components.Analyse;
using Core.Domain.DevOps.Components.Build;
using Core.Domain.DevOps.Components.Debug;
using Core.Domain.DevOps.Components.Deploy;
using Core.Domain.DevOps.Components.Package;
using Core.Domain.DevOps.Components.Sources;
using Core.Domain.DevOps.Components.Test;
using Core.Domain.DevOps.Components.Utility;

namespace Core.Domain.DevOps.Visitors;

public class OperationCountVisitor: IDevOpsVisitor
{
    public int JobCount = 0;
    public int OperationCount = 0;

    private void IncrementJobCount()
    {
        JobCount++;
    }
    private void IncrementOperationCount()
    {
        OperationCount++;
    }

    public void ResetCounter()
    {
        JobCount = 0;
        OperationCount = 0;
    }
    
    //VISIT METHODS
    public void Visit(DevelopmentPipeline developmentPipeline)
    {
        developmentPipeline.PipelineStatus = DevelopmentPipeline.PipelineStatusEnum.InProgress;

        try
        {
            foreach (var item in developmentPipeline.Jobs)
            {
                item.Accept(this);
            }

            developmentPipeline.PipelineStatus = DevelopmentPipeline.PipelineStatusEnum.Success;
        }
        catch
        {
            developmentPipeline.PipelineStatus = DevelopmentPipeline.PipelineStatusEnum.Failed;
            developmentPipeline.NotifyUsers();
        }
    }

    public void Visit(Job job)
    {
        IncrementJobCount();
        foreach (var item in job.Children)
        {
            item.Accept(this);
        }
    }

    public void Visit(CopyFile copyFile)
    {
        IncrementOperationCount();
    }

    public void Visit(MoveFile moveFile)
    {
        IncrementOperationCount();
    }

    public void Visit(DeleteFile deleteFile)
    {
        IncrementOperationCount();
    }

    public void Visit(RustBuild rustBuild)
    {
        IncrementOperationCount();
    }

    public void Visit(MavenBuild rustBuild)
    {
        IncrementOperationCount();
    }

    public void Visit(DotNetBuild rustBuild)
    {
        IncrementOperationCount();
    }

    public void Visit(ChangeDirectory rustBuild)
    {
        IncrementOperationCount();
    }

    public void Visit(Script script)
    {
        foreach (var item in script.Operations)
        {
            item.Accept(this);
        }
    }

    public void Visit(NuGetInstall nuGetInstall)
    {
        IncrementOperationCount();
    }

    public void Visit(NpmInstall npmInstall)
    {
        IncrementOperationCount();
    }

    public void Visit(AptInstall aptInstall)
    {
        IncrementOperationCount();
    }

    public void Visit(HerokuDeploy herokuDeploy)
    {
        IncrementOperationCount();
    }

    public void Visit(AzureDeploy azureDeploy)
    {
        IncrementOperationCount();
    }

    public void Visit(AwsDeploy awsDeploy)
    {
        IncrementOperationCount();
    }

    public void Visit(SonarScannerBegin sonarScannerBegin)
    {
        IncrementOperationCount();
    }

    public void Visit(SonarScannerEnd sonarScannerEnd)
    {
        IncrementOperationCount();
    }

    public void Visit(GitPull gitPull)
    {
        IncrementOperationCount();
    }

    public void Visit(GitCheckout gitCheckout)
    {
        IncrementOperationCount();
    }

    public void Visit(GitClone gitClone)
    {
        IncrementOperationCount();
    }

    public void Visit(DotNetTest dotNetTest)
    {
        IncrementOperationCount();
    }

    public void Visit(MochaTest mochaTest)
    {
        IncrementOperationCount();
    }

    public void Visit(NUnitTest nUnitTest)
    {
        IncrementOperationCount();
    }

    public void Visit(Crash crash)
    {
        throw new Exception("Intentional Exception");
    }
}