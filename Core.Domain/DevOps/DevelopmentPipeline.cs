using Core.Domain.DevOps.Components;
using Core.Domain.DevOps.Visitors;
using Core.Domain.TypesOfUser;

namespace Core.Domain.DevOps;

public class DevelopmentPipeline
{
    public string Name;
    public IList<Job> Jobs;
    public PipelineStatusEnum PipelineStatus = PipelineStatusEnum.NotStarted;
    public SprintBacklog? ParentSprintBacklog;

    public enum PipelineStatusEnum
    {
        NotStarted,
        InProgress,
        Success,
        Failed
    }

    public DevelopmentPipeline(string name)
    {
        Name = name;
        Jobs = new List<Job>();
    }

    public void addJob(Job job)
    {
        Jobs.Add(job);
    }

    public void removeJob(Job job)
    {
        Jobs.Remove(job);
    }

    public void accept(IDevOpsVisitor visitor)
    {
        visitor.Visit(this);
    }

    public void NotifyUsers()
    {
        if (ParentSprintBacklog != null)
        {
            foreach (var item in ParentSprintBacklog.Team)
            {
                if (PipelineStatus == PipelineStatusEnum.Failed && item is ScrumMaster) item.Notify();
            }
        }
    }
}