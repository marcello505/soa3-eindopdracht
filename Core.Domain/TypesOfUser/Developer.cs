﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfUser
{
    public class Developer : User
    {
        public Developer(string firstName, string lastName, string email) : base(firstName, lastName, email)
        {
        }
    }
}
