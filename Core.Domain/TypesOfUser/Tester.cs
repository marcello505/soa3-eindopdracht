﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfUser
{
    public class Tester : User
    {
        public Tester(string firstName, string lastName, string email) : base(firstName, lastName, email)
        {
        }
    }
}
