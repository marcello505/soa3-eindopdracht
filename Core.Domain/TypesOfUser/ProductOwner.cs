﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfUser
{
    public class ProductOwner : User
    {
        public ProductOwner(string firstName, string lastName, string email) : base(firstName, lastName, email)
        {
        }
    }
}
