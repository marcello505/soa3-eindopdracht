﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.Notifications;

namespace Core.Domain
{
    public abstract class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public IList<INotificationSubscriber> NotificationTypes { get; set; }

        protected User(string firstName, string lastName, string email)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            NotificationTypes = new List<INotificationSubscriber>();
        }

        public void AddNotificationType(INotificationSubscriber notificationSubscriber)
        {
            NotificationTypes.Add(notificationSubscriber);
        }

        public void RemoveNotificationType(INotificationSubscriber notificationSubscriber)
        {
            NotificationTypes.Remove(notificationSubscriber);
        }


        public void Notify()
        {
            foreach (var item in NotificationTypes)
            {
                item.Notify();
            }
        }
    }
}
