﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Forum
{
    public class Message
    {
        public string Comment { get; set; }
        public User User { get; set; }

        public Message(string comment, User user)
        {
            Comment = comment;
            User = user;
        }
    }
}
