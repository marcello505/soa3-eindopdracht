﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Forum
{
    public class Thread
    {
        public List<Message> Messages { get; set; }
        public IList<User> SubscribedUsers { get; }

        public Thread(Message message)
        {
            SubscribedUsers = new List<User>();
            Messages = new List<Message>();
            AddMessage(message);
        }

        public void AddMessage(Message newMessage)
        {
            Messages.Add(newMessage);
            NotifySubscribers();
            Subscribe(newMessage.User);
        }

        private void NotifySubscribers()
        {
            var lastPostUser = Messages.Last().User;
            foreach (var user in SubscribedUsers)
            {
                if(user != lastPostUser) user.Notify();
            }
        }

        public void Subscribe(User user)
        {
            if(!SubscribedUsers.Contains(user)) SubscribedUsers.Add(user);
        }

        public void Unsubscribe(User user)
        {
            SubscribedUsers.Remove(user);
        }
    }
}
