﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReviewSprints
{
    public abstract class ReviewSprintState
    {
        public ReviewSprint Sprint { get; set; }

        protected ReviewSprintState(ReviewSprint sprint)
        {
            Sprint = sprint;
        }

        public virtual void Created() { }
        public virtual void InProgress(User user) { }
        public virtual void Finished(DateTime date) { }
        public virtual void Review() { }
        public virtual void Completed(User user) { }
    }
}
