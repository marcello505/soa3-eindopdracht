﻿using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReviewSprints
{
    public class CreatedState : ReviewSprintState
    {
        public CreatedState(ReviewSprint sprint) : base(sprint)
        {
        }

        public override void InProgress(User user)
        {
            if (user.GetType() == typeof(ScrumMaster))
            {
                Sprint.State = new InProgressState(Sprint);
            }
        }
    }
}
