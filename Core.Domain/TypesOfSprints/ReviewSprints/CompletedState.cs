﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReviewSprints
{
    public class CompletedState : ReviewSprintState
    {
        public CompletedState(ReviewSprint sprint) : base(sprint)
        {
        }
    }
}
