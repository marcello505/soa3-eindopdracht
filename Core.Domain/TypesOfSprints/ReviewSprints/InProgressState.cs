﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReviewSprints
{
    public class InProgressState : ReviewSprintState
    {
        public InProgressState(ReviewSprint sprint) : base(sprint)
        {
        }

        public override void Finished(DateTime date)
        {
            if (Sprint.EndDate <= date)
            {
                Sprint.State = new FinishedState(Sprint);
            }               
        }
    }
}
