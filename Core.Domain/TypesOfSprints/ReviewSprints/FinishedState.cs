﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReviewSprints
{
    public class FinishedState : ReviewSprintState
    {
        public FinishedState(ReviewSprint sprint) : base(sprint)
        {
        }

        public override void Review()
        {
            Sprint.State = new ReviewState(Sprint);
        }
    }
}
