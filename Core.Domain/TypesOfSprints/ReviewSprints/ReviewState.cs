﻿using Core.Domain.DevOps;
using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReviewSprints
{
    public class ReviewState : ReviewSprintState
    {
        public ReviewState(ReviewSprint sprint) : base(sprint)
        {
        }

        public override void Completed(User user)
        {
            if(user.GetType() == typeof(ScrumMaster) && Sprint.IsReviewed && Sprint.Pipeline != null)
            {
                Sprint.StartPipeline();

                if(Sprint.Pipeline.PipelineStatus == DevelopmentPipeline.PipelineStatusEnum.Success)
                {                   
                    Sprint.State = new CompletedState(Sprint);
                }
            }            
        }
    }
}
