﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReviewSprints
{
    public class ReviewSprint : SprintBacklog
    {
        public ReviewSprintState State { get; set; }
        public bool IsReviewed { get; set; }
        
        public ReviewSprint(int sprintNumber, DateTime startDate, DateTime endDate) : base(sprintNumber, startDate, endDate)
        {
            State = new CreatedState(this);
        }

        public void Reviewed()
        {
            IsReviewed = true;
        }

        public override bool IsStatusCreated()
        {
            return State.GetType() == typeof(CreatedState);
        }

        public override bool IsStatusFinished()
        {
            return State.GetType() != typeof(CreatedState) && State.GetType() != typeof(InProgressState);
        }
    }
}
