﻿using Core.Domain.ProductBacklogItems;
using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.DevOps;
using Core.Domain.DevOps.Visitors;
using Core.Domain.TypesOfSprints.ReviewSprints;

namespace Core.Domain
{
    public abstract class SprintBacklog
    {
        public int SprintNumber { get; set; }
        public List<ProductBacklogItem> Items { get; set; }
        public List<User> Team { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Project? RootProject { get; set; }
        public DevelopmentPipeline? Pipeline { get; set; }

        protected SprintBacklog(int sprintNumber, DateTime startDate, DateTime endDate)
        {
            SprintNumber = sprintNumber;
            Items = new List<ProductBacklogItem>();
            Team = new List<User>();
            StartDate = startDate;
            EndDate = endDate;
        }

        public void AddItem(ProductBacklogItem item)
        {
            if (!IsStatusFinished())
            {
                item.SprintBacklog = this;
                Items.Add(item);
            }           
        }

        public void SetPipeline(DevelopmentPipeline developmentPipeline)
        {
            developmentPipeline.ParentSprintBacklog = this;
            Pipeline = developmentPipeline;
        }

        public void StartPipeline()
        {
            if (Pipeline != null)
            {
                var operationCountVisitor = new OperationCountVisitor();
                Pipeline.accept(operationCountVisitor);
            }
        }

        public abstract bool IsStatusCreated();

        public abstract bool IsStatusFinished();

        public void SetSprintNumber(int number)
        {
            if (IsStatusCreated())
            {
                SprintNumber = number;
            }
        }

        public void SetStartDate(DateTime newDate)
        {
            if (IsStatusCreated())
            {
                StartDate = newDate;
            }
        }

        public void SetEndDate(DateTime newDate)
        {
            if (IsStatusCreated())
            {
                EndDate = newDate;
            }
        }

        public void AddTeamMember(User user)
        {
            Team.Add(user);
        }
    }
}
