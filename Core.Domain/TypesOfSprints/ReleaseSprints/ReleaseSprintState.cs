﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReleaseSprints
{
    public class ReleaseSprintState
    {
        public ReleaseSprint Sprint { get; set; }

        public ReleaseSprintState(ReleaseSprint sprint)
        {
            Sprint = sprint;
        }

        public virtual void Created() { }
        public virtual void InProgress(User user) { }
        public virtual void Finished(DateTime date) { }
        public virtual void Release() { }
        public virtual void Completed() { }
        public virtual void Cancelled() { }
    }
}
