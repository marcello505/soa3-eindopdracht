﻿using Core.Domain.DevOps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReleaseSprints
{
    public class ReleaseState : ReleaseSprintState
    {
        public ReleaseState(ReleaseSprint sprint) : base(sprint)
        {
        }
        public override void Completed()
        {
            if (Sprint.Pipeline != null && Sprint.Pipeline.PipelineStatus == DevelopmentPipeline.PipelineStatusEnum.Success)
            {
                Sprint.State = new CompletedState(Sprint);
            }
        }

        public override void Cancelled()
        {
            Sprint.State = new CancelledState(Sprint);
        }
    }
}
