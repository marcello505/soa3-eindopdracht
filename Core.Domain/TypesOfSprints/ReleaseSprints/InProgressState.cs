﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReleaseSprints
{
    public class InProgressState : ReleaseSprintState
    {
        public InProgressState(ReleaseSprint sprint) : base(sprint)
        {
        }

        public override void Finished(DateTime date)
        {
            if (Sprint.EndDate <= date)
            {
                Sprint.State = new FinishedState(Sprint);
            }
        }
    }
}
