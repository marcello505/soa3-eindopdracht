﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReleaseSprints
{
    public class CompletedState : ReleaseSprintState
    {
        public CompletedState(ReleaseSprint sprint) : base(sprint)
        {
        }
    }
}
