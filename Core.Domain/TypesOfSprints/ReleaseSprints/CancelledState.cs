﻿using Core.Domain.DevOps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReleaseSprints
{
    public class CancelledState : ReleaseSprintState
    {
        public CancelledState(ReleaseSprint sprint) : base(sprint)
        {
        }

        public override void Release()
        {
            Sprint.State = new ReleaseState(Sprint);
            Sprint.StartPipeline();

            if (Sprint.Pipeline != null && Sprint.Pipeline.PipelineStatus == DevelopmentPipeline.PipelineStatusEnum.Success)
            {
                Sprint.State = new CompletedState(Sprint);
            }
            else
            {
                Sprint.State = new CancelledState(Sprint);
            }
        }
    }
}
