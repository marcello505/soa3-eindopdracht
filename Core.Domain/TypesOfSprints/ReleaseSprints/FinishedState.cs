﻿using Core.Domain.DevOps;
using Core.Domain.DevOps.Visitors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.TypesOfUser;

namespace Core.Domain.TypesOfSprints.ReleaseSprints
{
    public class FinishedState : ReleaseSprintState
    {
        public FinishedState(ReleaseSprint sprint) : base(sprint)
        {
        }

        public override void Release()
        {
            Sprint.State = new ReleaseState(Sprint);
            Sprint.StartPipeline();
  
            if (Sprint.Pipeline != null && Sprint.Pipeline.PipelineStatus == DevelopmentPipeline.PipelineStatusEnum.Success)
            {
                NotifyProductOwnerAndScrumMaster();
                Sprint.State = new CompletedState(Sprint);
            } 
            else
            {
                NotifyProductOwnerAndScrumMaster();
                Sprint.State = new CancelledState(Sprint);
            }                  
        }

        private void NotifyProductOwnerAndScrumMaster()
        {
            //Notify Scrum Masters
            foreach (var user in Sprint.Team)
            {
                if(user is ScrumMaster) user.Notify();
            }
            
            //Notify productowner
            if(Sprint.RootProject != null && Sprint.RootProject.ProductOwner != null) Sprint.RootProject.ProductOwner.Notify();
        }
    }
}
