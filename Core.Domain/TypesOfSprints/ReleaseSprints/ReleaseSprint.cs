﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.TypesOfSprints.ReleaseSprints
{
    public class ReleaseSprint : SprintBacklog
    {
        public ReleaseSprintState State { get; set; }
        
        public ReleaseSprint(int sprintNumber, DateTime startDate, DateTime endDate) : base(sprintNumber, startDate, endDate)
        {
            State = new CreatedState(this);
        }

        public override bool IsStatusCreated()
        {
            return State.GetType() == typeof(CreatedState);
        }

        public override bool IsStatusFinished()
        {
            return State.GetType() != typeof(CreatedState) && State.GetType() != typeof(InProgressState);
        }
    }
}
