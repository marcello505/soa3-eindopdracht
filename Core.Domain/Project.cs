﻿using Core.Domain.ProductBacklogItems;
using Core.Domain.TypesOfSprints.ReleaseSprints;
using Core.Domain.TypesOfSprints.ReviewSprints;
using Core.Domain.TypesOfUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public class Project
    {
        public string ProjectName { get; set; }
        public ProductOwner? ProductOwner { get; set; }
        public string Version { get; set; }
        public List<ProductBacklogItem> ProductBacklog { get; set; }
        public IList<SprintBacklog> SprintBacklogs { get; set; }

        public Project(string projectName)
        {
            ProjectName = projectName;
            Version = "0.0";
            ProductBacklog = new List<ProductBacklogItem>();
            SprintBacklogs = new List<SprintBacklog>();
        }

        public void AssignProductOwner(ProductOwner productOwner)
        {
            ProductOwner = productOwner;
        }

        public void UpdateVersion(string newVersion)
        {
            Version = newVersion;
        }

        public void AddItem(ProductBacklogItem item)
        {
            ProductBacklog.Add(item);
        }

        public void CreateNewReviewSprint(int number, DateTime startDate, DateTime endDate)
        {
            SprintBacklog Sprint = new ReviewSprint(number, startDate, endDate);
            AddSprint(Sprint);
        }

        public void CreateNewReleaseSprint(int number, DateTime startDate, DateTime endDate)
        {
            SprintBacklog Sprint = new ReleaseSprint(number, startDate, endDate);
            AddSprint(Sprint);
        }

        public void AddSprint(SprintBacklog sprint)
        {
            sprint.RootProject = this;
            SprintBacklogs.Add(sprint);
        }

        public void ItemToSprint(SprintBacklog sprint, ProductBacklogItem item)
        {
            sprint.Items.Add(item);
        }
    }
}
